import React, { useState, useEffect } from 'react';

import { Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import {
  Container,
  Card,
  CardContent
} from '@material-ui/core';

import '@brainhubeu/react-carousel/lib/style.css';
import clsx from 'clsx';

import Navbar from '../../components/Navbar'
import Typography from '@material-ui/core/Typography';

import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import FormControl from '@material-ui/core/FormControl';
import Avatar from '@material-ui/core/Avatar';

import OutlinedInput from '@material-ui/core/OutlinedInput';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import { Auth } from 'aws-amplify';

const useStyles = makeStyles((theme) => ({
  root: {
    // display: 'flex',
    flexGrow: 1
  },
  cardroot: {
    borderTop: '3px solid #0A67D4'
  },
  inline: {
    display: 'inline',
  },
  heading: {
    fontSize: theme.typography.pxToRem(25),
    flexBasis: '33.33%',
    flexShrink: 0,
  },
  large: {
    width: theme.spacing(25),
    height: theme.spacing(25),
  },

}))


export default function Profile(props) {
  const classes = useStyles()

  return (
    <div className={classes.root}>
      <Navbar />
      <Container 
      maxWidth="md"
      direction="row"
      justify="center"
      spacing={4}
      >
        <Card className={classes.cardroot}>
          <CardContent>
            <Typography variant="h4">
              Account Settings
            </Typography>
            <br />
            <br />
            <SimpleAccordion/>
          </CardContent>
        </Card>
      </Container>
    </div >
  );
}





function SimpleAccordion() {
  const classes = useStyles();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");

  async function currentUserInfo() {
    try {
      let response = await Auth.currentUserInfo();
      console.log(response.attributes)
      console.log('success')
      setUsername(response.attributes.name);
      setEmail(response.attributes.email);
      setPhone(response.attributes.phone_number);

    } catch (error) {
        console.log('error current userinfo: ', error);
    }
  }

  useEffect(() => {
    currentUserInfo()
  });

  return (
    <div>
      <Accordion  >
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
          style={{backgroundColor:'#F7F7FB'}}
        >
          <Typography className={classes.heading}>User Profile</Typography>
        </AccordionSummary>
        <Divider />
        <AccordionDetails style={{minHeight:'100px'}} >

        <List className={classes.root}>
          <ListItem >
            <Container style={{ justifyContent: "center", display: "flex" }}>
                <Avatar src="images/uxceo-128.jpg"   className={classes.large}/>
            </Container>
          </ListItem>
          <ListItem>
            <Grid container spacing={3} alignItems="center">
            <Grid item xs={6}>
            <Typography  variant="h6">Username</Typography>
            </Grid>
            <Grid item xs={6}>
            <FormControl className={clsx(classes.margin, classes.textField)} variant="outlined">
              <OutlinedInput
                value={username}  
                onChange={(e) => setUsername(e.target.value)}
              />
            </FormControl>
            </Grid>
            </Grid>
          </ListItem>
          <ListItem>
            <Grid container spacing={3} alignItems="center">
            <Grid item xs={6}>
            <Typography variant="h6">Password</Typography>
            </Grid>
            <Grid item xs={6}>
            <FormControl className={clsx(classes.margin, classes.textField)} variant="outlined">
              <OutlinedInput
                type="password"
                value={password}  
                onChange={(e) => setPassword(e.target.value)}
              />
            </FormControl>
            </Grid>
            </Grid>
          </ListItem>
          <ListItem>
            <Grid container spacing={3} alignItems="center">
            <Grid item xs={6}>
            <Typography variant="h6">Mail</Typography>
            </Grid>
            <Grid item xs={6}>
            <FormControl className={clsx(classes.margin, classes.textField)} variant="outlined">
              <OutlinedInput
                value={email}  
                onChange={(e) => setEmail(e.target.value)}
              />
            </FormControl>
            </Grid>
            </Grid>
          </ListItem>
          <ListItem>
            <Grid container spacing={3} alignItems="center">
            <Grid item xs={6} >
            <Typography variant="h6">Phone</Typography>
            </Grid>
            <Grid item xs={6}>
            <FormControl className={clsx(classes.margin, classes.textField)} variant="outlined">
              <OutlinedInput
                value={phone}  
                onChange={(e) => setPhone(e.target.value)}

              />
            </FormControl>
            </Grid>
            </Grid>
          </ListItem>
        </List>

      
        </AccordionDetails>
      </Accordion>









      <Accordion >
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel2a-content"
          id="panel2a-header"
          style={{backgroundColor:'#F7F7FB'}}
        >
          <Typography className={classes.heading}>Mandate</Typography>
        </AccordionSummary>
        <Divider />

        <AccordionDetails style={{minHeight:'100px', margin:'10px'}} >
          
        <Typography variant="h6">+Add/Edit Mandate </Typography>

        </AccordionDetails>
      </Accordion>


      <Accordion >
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel3a-content"
          id="panel3a-header"
          style={{backgroundColor:'#F7F7FB'}}
        >
          <Typography className={classes.heading}>Portfolio</Typography>
        </AccordionSummary>
        <Divider />
        <AccordionDetails style={{minHeight:'100px', margin:'10px'}} >
        <Typography variant="h6">+Add/Edit Portfolio </Typography >
        </AccordionDetails>
      </Accordion>


    </div>
  );
}
