import ps1 from '../../assets/ps1.jpg'
import ps2 from '../../assets/ps2.jpg'
import ps3 from '../../assets/ps3.jfif'
import ps4 from '../../assets/ps4.jpg'
import ps5 from '../../assets/environment.jpg'
import ps6 from '../../assets/ps6.jpg'

export const loginData = {
    username:'Pradeep'
}

export const popularSearches = [
    { id: 1, title: 'GHG Emissions', imgURL: ps1, key:'emission', search: "How much is GHG emissions for company AB" },
    { id: 2, title: 'Peer Comparison', imgURL: ps2,key:'peer', search: "How much is GHG emissions for company AB" },
    { id: 3, title: 'Target Vs ActuAL', imgURL: ps3,key:'target', search: "How much is GHG emissions for company AB" },
    { id: 4, title: 'Auto Industry', imgURL: ps4,key:'industry', search: "How much is GHG emissions for company AB" },
    { id: 5, title: 'Environment', imgURL: ps5,key:'environment', search: "How much is GHG emissions for company AB" },
    { id: 6, title: 'Strategy', imgURL: ps6,key:'strategy', search: "How much is GHG emissions for company AB" }
];

export const tableHeaders = [
    {
        label: '',
        key: 'label',
        sort: false
    },
    {
        label: '2017',
        key: '2017',
        sort: false
    },
    {
        label: '2018',
        key: '2018',
        sort: false
    },
    {
        label: '2019',
        key: '2019',
        sort: false
    },
    {
        label: '2020',
        key: '2020',
        sort: false
    },
    {
        label: '2021',
        key: '2021',
        sort: false
    }
]

export const searchData = [
    {
        year: '2017',
        emission: '22.40',
        revenue: '2500',
        ghgByRevenue: '0.90%'
    },
    {
        year: '2018',
        emission: '21',
        revenue: '2350',
        ghgByRevenue: '0.89%'
    },
    {
        year: '2019',
        emission: '19',
        revenue: '2150',
        ghgByRevenue: '0.88%'
    },
    {
        year: '2020',
        emission: '18',
        revenue: '1950',
        ghgByRevenue: '0.92%'
    },
    {
        year: '2021',
        emission: '15',
        revenue: '1700',
        ghgByRevenue: '0.88%'
    },
]

export const emissionTableData =[
    {
        label:'GHG Emissions',
        '2017':'22.4',
        '2018':'21',
        '2019':'19',
        '2020':'18',
        '2021':'15',
    },
    {
        label:'Revenue',
        '2017':'2500',
        '2018':'2350',
        '2019':'2150',
        '2020':'1950',
        '2021':'1700',
    },
    {
        label:'GHG / Revenue',
        '2017':'0.90%',
        '2018':'0.89%',
        '2019':'0.88%',
        '2020':'0.92%',
        '2021':'0.88%',
    },
]

export const emissionGraphData = [
    {
        name: '2017',
        value: 22.4
    },
    {
        name: '2018',
        value: 21
    },
    {
        name: '2019',
        value: 19
    },
    {
        name: '2020',
        value: 18
    },
    {
        name: '2021',
        value: 15
    }
]

export const revenueGraphData = [
    {
        name: '2017',
        value: 0.90
    },
    {
        name: '2018',
        value: 0.89
    },
    {
        name: '2019',
        value: 0.88
    },
    {
        name: '2020',
        value: 0.92
    },
    {
        name: '2021',
        value: 0.88
    }
]

export const corouselData = [{
    imgURL: 'https://images.unsplash.com/photo-1560977490-bf4117146fc9?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1229&q=80',
    imgHeight: 250,
    imgAlt: 'Background Image',
    title: `Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging across all continents except Antarctica`
}, {
    imgURL: 'https://images.unsplash.com/photo-1560977490-bf4117146fc9?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1229&q=80',
    imgHeight: 250,
    imgAlt: 'Background Image',
    title: `Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging across all continents except Antarctica`
}, {
    imgURL: 'https://images.unsplash.com/photo-1560977490-bf4117146fc9?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1229&q=80',
    imgHeight: 250,
    imgAlt: 'Background Image',
    title: `Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging across all continents except Antarctica`
}, {
    imgURL: 'https://images.unsplash.com/photo-1560977490-bf4117146fc9?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1229&q=80',
    imgHeight: 250,
    imgAlt: 'Background Image',
    title: `Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging across all continents except Antarctica`
}]

export const ghgCompanyDetails = "Company AB explores for and produces crude oil and natural gas in the United States and internationally. It operates through Upstream, Downstream, and Chemical segments. The company is also involved in the manufacture, trade, transport, and sale of crude oil, natural gas, petroleum products, petrochemicals, and other specialty products; and manufactures and sells petrochemicals, including olefins, polyolefins, aromatics, and various other petrochemicals. The company was founded in 1870 and is based in Irving, Texas."