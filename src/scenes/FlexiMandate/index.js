import React, { useState } from 'react';

import { makeStyles } from '@material-ui/core/styles';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';

import Navbar from '../../components/Navbar'
import CustomDataTable from '../../components/CustomDataTable';
import DropDownMenu from '../../components/DropDownMenu';

import { DATA as portFolioData } from '../AddPortFolio/PortFolioData';
import { MANDATEDATA } from '../AddMandate/mandateData';

import AddCompany from '../AddCompany';
import AddOrEditPortfolio from '../AddPortFolio';
import AddOrEditMandate from '../AddMandate';

import { data, tableHeaders } from './fleximandateJsons.js'

const useStyles = makeStyles((theme) => ({
    root: {
        // display: 'flex',
        flexGrow: 1,
    },
    main: {
        backgroundColor: '#F7F7FB',
        minHeight: '100vh',
        height: '100%',
    },
    tableDiv: {
        padding: theme.spacing(3)
    },
    addCompanyButton: {
        textTransform: 'none',
        fontSize: 14,
        // marginRight: theme.spacing(3)
    },
}))

export default function FlexiMandate(props) {
    const classes = useStyles()

    const [order, setOrder] = React.useState('asc');
    const [orderBy, setOrderBy] = React.useState('environment');
    const [tableData, setTableData] = React.useState(data);
    const [filter, setFilter] = React.useState({
        portfolio: {},
        mandate: {}
    });


    const handleRequestSort = (event, property) => {
        const isAsc = orderBy === property && order === 'asc';
        setOrder(isAsc ? 'desc' : 'asc');
        setOrderBy(property);
    };

    const makeTableData = (dataArray) => {
        return dataArray.map(row => {
            return {
                ...row,
                action: <DeleteOutlineIcon color='error' />
            }
        })
    }

    React.useEffect(() => {
        let d = makeTableData(data)
        setTableData(d)
    }, [])

    const onAddCompanyHandler = (event, companies) => {
        console.log("thee are selected companies in flexi mandate", companies);
        if (companies && companies.length) {
            let d = companies.map(comp => {
                return {
                    ...comp,
                    environment: 22,
                    socialcapital: 12,
                    humancapital: 33,
                    businessmodel: 10,
                    leadership: 15,
                    total: 52.15
                }
            })
            d = makeTableData(d)
            setTableData(d)
        } else setTableData([])
    }

    const onPortfolioChange = (portfolio) => {
        setFilter({ ...filter, portfolio: portfolio })
    }

    const onMandateChange = (mandate) => {
        setFilter({ ...filter, mandate: mandate })
    }

    const ActionComponent = (props) => {
        return (
            <>
                <AddCompany defaultCompanies={data} addHandler={onAddCompanyHandler} />
                <DropDownMenu
                    title="Portfolio"
                    value={filter.portfolio.label}
                    options={[
                        {
                            id: 'addportfolio',
                            label: <AddOrEditPortfolio />,
                        },
                        ...portFolioData.map(item => ({ id: item.portfolioid, label: item.portfolioname, value: item.portfolioname })),

                    ]}
                    onClick={() => console.log("add portfolio menu btn clicked")}
                    onChange={onPortfolioChange}
                />
                <DropDownMenu
                    title="Mandate"
                    value={filter.mandate.label}
                    options={[
                        {
                            id: 'addmandate',
                            label: <AddOrEditMandate />,
                        },
                        ...MANDATEDATA.map(item => ({ id: item.mandateid, label: item.mandatename, value: item.mandatename })),

                    ]}
                    onClick={() => console.log("mandate menu btn clicked")}
                    onChange={onMandateChange}
                />
            </>
        )
    }

    return (
        <div className={classes.root} >
            <Navbar />
            <div className={classes.tableDiv}>
                <CustomDataTable
                    title='Nirva Flexi-Mandate'
                    headerData={tableHeaders}
                    tableData={tableData}
                    actions={<ActionComponent />}
                    sortable={true}
                    order={order}
                    orderBy={orderBy}
                    onRequestSort={handleRequestSort}
                />
            </div>
        </div >
    )
}