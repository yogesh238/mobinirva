import React from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';

import {
	Container,
	Button,
	Link,
	Grid,
	Card,
	Slider,
	Typography,
	CardContent,
	Paper
} from '@material-ui/core';

import { COMPANY_STATS } from './chartData';
import Navbar from '../../components/Navbar'
import CandleStickChart from '../../components/CandleStickChart';

const useStyles = makeStyles((theme) => ({
	root: {
		// display: 'flex',
		flexGrow: 1,
	},

	tableDiv: {
		padding: theme.spacing(3)
	},


	formControl: {
		margin: theme.spacing(1),
		minWidth: 120,
	},
	selectEmpty: {
		marginTop: theme.spacing(2),
	},
	title: {
		flex: '1 2 80%',
		color: '#131111',
		fontSize: 22,
		fontWeight: 'bold',
		marginBottom: theme.spacing(2),
		padding: 25
	},
	image: {
		width: '100%',
	},
	ccTitle: {
		fontSize: 16,
		fontWeight: '500',
		color: '#000000',
	},
	links: {
		'& > * + *': {
			marginLeft: theme.spacing(2),
		},
	},
	controversies: {
		padding: '3px',
	}

}))

export const controversies = [{
	imgURL: 'https://images.unsplash.com/photo-1560977490-bf4117146fc9?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1229&q=80',
	imgHeight: 250,
	imgAlt: 'Background Image',
	title: `Premature factory re-opening leads to controversy`,
	description: `Company ABC involved in controversy with authorities over reopening its faulty healt concerns.`,
	list_actions: ['ABC', 'Human Capital', 'Medium', 'Negative']
}, {
	imgURL: 'https://images.unsplash.com/photo-1560977490-bf4117146fc9?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1229&q=80',
	imgHeight: 250,
	imgAlt: 'Background Image',
	title: `Major Auto Company recalls model Y`,
	description: `Company XYZ recalls 500,000 units of its model Y to address faulty steering column`,
	list_actions: ['ABC', 'Governance', 'Medium', 'Negative']
}, {
	imgURL: 'https://images.unsplash.com/photo-1560977490-bf4117146fc9?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1229&q=80',
	imgHeight: 250,
	imgAlt: 'Background Image',
	title: `Environment liability provisions on the rise`,
	description: `Company MNC sets aside $8 million as reserves for environment liabilities in FY20`,
	list_actions: ['MNC', 'Environment', 'High', 'Negative']
}, {
	imgURL: 'https://images.unsplash.com/photo-1560977490-bf4117146fc9?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1229&q=80',
	imgHeight: 250,
	imgAlt: 'Background Image',
	title: `Negotiation with unions hits a stalemate`,
	description: `Company RY reports work stoppage in its plant due to unfavourable negotiation outcomes with its employee unions`,
	list_actions: ['RY', 'Social Capital', 'Medium', 'Negative']

}, {
	imgURL: 'https://images.unsplash.com/photo-1560977490-bf4117146fc9?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1229&q=80',
	imgHeight: 250,
	imgAlt: 'Background Image',
	title: `NEV capex explodes`,
	description: `Company BMG to maintain high capex spending on New Energy Vehicles (NEV) technology that will enable it to expand on its NEV credits`,
	list_actions: ['BMG', 'Environment', 'Medium', 'Positive']

}]

const marks = [
	{
		value: 0,
		label: 'lowest',
	},
	{
		value: 25,
		label: 'low',
	},
	{
		value: 50,
		label: 'medium',
	},
	{
		value: 75,
		label: 'high',
	},

	{
		value: 100,
		label: 'highest',
	},
];

function valuetext(value) {
	return `${value}°C`;
}

function valueLabelFormat(value) {
	return marks.findIndex((mark) => mark.value === value) + 1;
}

const PrettoSlider = withStyles({
	root: {
		color: '#52af77',
		height: 8,
	},
	thumb: {
		height: 24,
		width: 24,
		backgroundColor: '#fff',
		border: '2px solid currentColor',
		marginTop: -8,
		marginLeft: -12,
		'&:focus, &:hover, &$active': {
			boxShadow: 'inherit',
		},
	},
	active: {},
	valueLabel: {
		left: 'calc(-50% + 4px)',
	},
	track: {
		height: 8,
		borderRadius: 4,
	},
	rail: {
		height: 8,
		borderRadius: 4,
	},
})(Slider);

export default function FlexiMandate(props) {
	const classes = useStyles()
	const preventDefault = (event) => event.preventDefault();

	return (
		<div className={classes.root} >
			<Navbar />
			<Container
				maxWidth="lg"
				spacing={2}
			>
				<Card>
					<CardContent>
						<Grid container spacing={3}>
							<Grid item xs={7}>
								<Grid container spacing={3} >
									<Grid item xs={12}>
										<Typography variant="h6">Nirva Controversy Module</Typography>
									</Grid>
									<Grid item xs={12}>
										<Button variant="contained" color="primary" >Portfolio</Button>
										<Button variant="contained" color="primary" style={{ marginLeft: '10px' }}>Company</Button>
									</Grid>
									<Grid item xs={12}>
										<Typography component="div">
											<Grid container spacing={1} style={{ padding: '20px' }}>
												<Grid item xs={3}><Typography variant="h6">Category</Typography></Grid>
												<Grid item xs={9} >
													<PrettoSlider
														style={{ color: 'blue' }}
														aria-label="pretto slider"
														track="inverted"
														defaultValue={50}
														valueLabelFormat={valueLabelFormat}
														getAriaValueText={valuetext}
														aria-labelledby="discrete-slider-restrict"
														step={null}
														valueLabelDisplay="off"
														marks={marks}
													/>
												</Grid>
											</Grid>
										</Typography>
									</Grid>
									<Grid item xs={12}>
										<Typography variant="h6">Company H</Typography>
										<Paper>
											<CandleStickChart data={COMPANY_STATS} />
										</Paper>
									</Grid>
								</Grid>
							</Grid>
							<Grid item xs={5}>
								<Typography variant="h6">ESG Investing Headlines</Typography>

								{controversies && controversies.map((item, index) => (
									<Card style={{ margin: '25px' }}>
										<CardContent>
											<Grid container spacing={3} className={classes.controversies} key={index}>
												<Grid item sx={6} md={4}>
													<img className={classes.image} alt={item.imgAlt} src={item.imgURL} />
												</Grid>
												<Grid item sx={6} md={8}>
													<Typography variant="body2">
														{item.title}
													</Typography>
													<Typography variant="caption" style={{ paddingTop: 5 }}>
														{`${item.description.substring(0, 80)}...`}
													</Typography>
												</Grid>

												<Grid item container sx={12} justify="space-between"  >
													{item.list_actions.map((item, index) => (
														<Link href="#" onClick={preventDefault} key={index} style={{ color: '#CFD6DE' }}>
															{item}
															{/* <Divider orientation="vertical"  /> */}
														</Link>
													))}
												</Grid>
											</Grid>
										</CardContent>
									</Card>
								))}
							</Grid>
						</Grid>
					</CardContent>
				</Card>
			</Container>
		</div >
	)
}