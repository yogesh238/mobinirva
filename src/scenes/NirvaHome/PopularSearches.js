import React from 'react';
import { Grid, Card, CardMedia, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    card: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5,
        cursor: "pointer",
        marginRight: 10,
        marginTop: 10,
        marginBottom: 10,
    },
    imgStyles: {
        backgroundSize: 'cover',
    },
    cardTitle: {
        position: 'absolute',
        color: '#FFFFFF',
        fontSize: 12,
        fontWeight: '400',
        textShadow: '2px 2px 6px #000000',
    }
}))

export default function PopularSearchImageCard(props) {
    const classes = useStyles();
    return (
        <Grid item lg={4} md={4} sm={6} xs={12} >
            <Card className={classes.card} onClick={(event) => props.onClick(event, props.searchWord, props.searchKey)}>
                <div class="overlay"></div>
                <CardMedia
                    component="img"
                    alt={props.imgAlt}
                    height={props.imgHeight}
                    image={props.imgURL}
                    title={props.imgAlt}
                    className={classes.imgStyles}
                />
                <div className={classes.cardTitle}>{props.title}</div>
            </Card>
        </Grid>
    )
}