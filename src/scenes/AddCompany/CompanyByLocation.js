import React from 'react';
// import PropTypes from 'prop-types';

import { makeStyles } from '@material-ui/core/styles';
import { FormControlLabel, Checkbox } from '@material-ui/core';
// import { CheckBox, CheckBoxOutlineBlank, } from '@material-ui/icons';

import CustomTreeView from '../../components/TreeView';


const buildComponent = ({ key, id, lable, isChecked, isFull, onClick, value }) => (
    <div key={key} id={key}  >
        <FormControlLabel
            control={
                <Checkbox
                    checked={isChecked}
                    onChange={(event) => onClick ? onClick(event, value) : null}
                    // name="checkedB"
                    size='small'
                    color={isFull ? 'primary' : 'default'}
                />
            }
            label={lable}
        />
        {/* {isChecked ?
            isFull ?
                <CheckBox htmlColor={'blue'} />
                :
                <CheckBox htmlColor={'grey'} />
            :
            <CheckBoxOutlineBlank htmlColor={'grey'} />
        }
        <span>{lable || ''}</span> */}
    </div>
)

const useStyles = makeStyles({
    root: {
        height: 110,
        flexGrow: 1,
        maxWidth: 400,
    },
});

const CompanyByLocationTree = (props) => {
    const classes = useStyles();

    const [dataForTree, setDataForTree] = React.useState([])
    const [defaultExpandNodes, setDefaultExpandNodes] = React.useState([])

    const makeData = (items, selected, companyClick, onParentClick, searchedCompany) => {

        if (items.length === 0 && !Array.isArray(items)) {
            return [];
        }
        return items.map((continent, i) => {
            let continentCompanies = [];
            let cont = {
                id: `cont-${i}-${continent.continentid}`,
                children: [],
            }

            //adding children to continent
            cont.children = continent.regions.map((reg, j) => {
                let newReg = {
                    id: `reg-${j}-${reg.regionid}`,
                    children: [],
                }

                let regionCompanies = []
                //adding children to region
                newReg.children = reg.countries.map((country, k) => {
                    let newCountry = {
                        id: `country-${k}-${country.countryid}`,
                        children: [],
                    }

                    //adding childrens to country
                    if (country && country.companies && country.companies.length) {
                        continentCompanies = continentCompanies.concat(country.companies)
                        regionCompanies = regionCompanies.concat(country.companies)
                        newCountry.children = country.companies.map((comp, l) => {
                            // let check = selected && selected.length && selected.includes(comp.companyid) ? true : false;
                            let check = selected && selected.length && selected.filter(company => company.companyid === comp.companyid).length > 0 ? true : false;
                            if (comp.companyid === searchedCompany.companyid) {
                                setDefaultExpandNodes([cont.id, newReg.id, newCountry.id, `comp-${l}-${comp.companyid}`])
                            }
                            return {
                                id: `comp-${l}-${comp.companyid}`,
                                name: buildComponent({
                                    id: comp.companyid,
                                    key: `comp-${l}-${comp.companyid}`,
                                    isChecked: check,
                                    isFull: true,
                                    lable: comp.companyname,
                                    onClick: companyClick,
                                    value: comp
                                }),
                                ischecked: check,
                                children: [],
                            }
                        })
                    }

                    //checking any children of country checked or not
                    let countryChecked = newCountry.children && newCountry.children.length && newCountry.children.some(child => child.ischecked);
                    newCountry.ischecked = countryChecked;

                    let everyCompanyChecked = newCountry.children && newCountry.children.length && newCountry.children.every(child => child.ischecked)
                    newCountry.isFull = everyCompanyChecked;

                    // adding country div
                    newCountry.name = buildComponent({
                        id: country.countryid,
                        key: `country-${k}-${country.countryid}`,
                        isChecked: countryChecked,
                        isFull: everyCompanyChecked,
                        lable: country.countryname,
                        onClick: onParentClick,
                        value: country.companies && country.companies.length ? country.companies : []
                    })
                    return newCountry;
                })

                //checking any children of region checked or not
                let regionChecked = newReg.children && newReg.children.length && newReg.children.some(child => child.ischecked);
                newReg.ischecked = regionChecked;

                let everyCountryChecked = newReg.children && newReg.children.length && newReg.children.every(child => child.isFull);
                newReg.isFull = everyCountryChecked

                //adding region div
                newReg.name = buildComponent({
                    id: reg.regionid,
                    key: `reg-${j}-${reg.regionid}`,
                    isChecked: regionChecked,
                    isFull: everyCountryChecked,
                    lable: reg.region,
                    onClick: onParentClick,
                    value: regionCompanies
                })
                return newReg;
            })

            //checking any children of continent checked or not
            let continentChecked = cont.children && cont.children.length && cont.children.some(child => child.ischecked);
            cont.ischecked = continentChecked;

            let everyRegionChecked = cont.children && cont.children.length && cont.children.every(child => child.isFull);
            cont.isFull = everyRegionChecked;

            //adding continent div
            cont.name = buildComponent({
                id: continent.continentid,
                key: `cont-${i}-${continent.continentid}`,
                isChecked: continentChecked,
                isFull: everyRegionChecked,
                lable: continent.continent,
                onClick: onParentClick,
                value: continentCompanies
            })
            return cont;
        });
    }

    const onNodeToggle = (event, nodes) => {
        // console.log("these are nodes", nodes);
        setDefaultExpandNodes(nodes)
    }

    React.useEffect(() => {
        let forTree = makeData(props.data, props.selectedCompanies, props.onCompanyClick, props.onParentClick, props.searchedCompany)
        setDataForTree(forTree)
    }, [props.data, props.selectedCompanies, props.searchedCompany])

    return (
        <CustomTreeView items={dataForTree} defaultExpandNodes={defaultExpandNodes} onNodeToggle={onNodeToggle} />
    )

}

export default CompanyByLocationTree;