import React from 'react';

import { withStyles, makeStyles } from '@material-ui/core/styles';
import { Paper, Grid, Typography, TableContainer, Table, TableHead, TableBody, TableRow, TableCell, TableSortLabel } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(1),
        borderRadius: 1,
        padding: theme.spacing(2, 2, 1, 2),
    },
    main: {
        display: 'flex',
        marginBottom: theme.spacing(1)
    },
    title: {
        flex: '1 2 80%',
        color: '#131111',
        fontSize: 22,
        fontWeight: 'bold',
        marginBottom: theme.spacing(2),
    },
    actionButtons: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end'
    }
}));

const StyledTableCell = withStyles((theme) => ({
    head: {
        // backgroundColor: theme.palette.common.black,
        color: '#0F51A9',
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
    root: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.action.hover,
        },
    },
}))(TableRow);

const RenderRow = (props) => {
    // const { enableClickOnCell, cellClick } = props;
    const classes = useStyles();
    return (
        props.keys.map((key, index) => (
            <StyledTableCell
                key={index}
            // onClick={
            //     enableClickOnCell && key === enableClickOnCell ?
            //         (event) => cellClick(event, props.data) : null} scope="row" className={classes.TableBody}
            >
                {props.data[key]}
            </StyledTableCell>
        ))
    )
}

function CustomDataTable(props) {
    const classes = useStyles()
    const { order, orderBy, onRequestSort, sortable } = props;

    const createSortHandler = (property) => (event) => {
        onRequestSort(event, property);
    };

    function descendingComparator(a, b, orderBy) {
        if (b[orderBy] < a[orderBy]) {
            return -1;
        }
        if (b[orderBy] > a[orderBy]) {
            return 1;
        }
        return 0;
    }

    function getComparator(order, orderBy) {
        return order === 'desc'
            ? (a, b) => descendingComparator(a, b, orderBy)
            : (a, b) => -descendingComparator(a, b, orderBy);
    }

    function stableSort(array, comparator) {
        const stabilizedThis = array.map((el, index) => [el, index]);
        stabilizedThis.sort((a, b) => {
            const order = comparator(a[0], b[0]);
            if (order !== 0) return order;
            return a[1] - b[1];
        });
        return stabilizedThis.map((el) => el[0]);
    }

    const RenderTableData = () => {
        let keys = props.headerData.map(header => header.key);
        return (
            (sortable ? stableSort(props.tableData, getComparator(order, orderBy)) : props.tableData).map((row, index) => (
                <StyledTableRow key={index}>
                    <RenderRow
                        key={index}
                        data={row}
                        keys={keys}
                    />
                </StyledTableRow>
            ))
        )
    }

    return (
        <Paper className={classes.paper} elevation={4}>
            <div className={classes.main}>
                <Grid container spacing={3}>
                    <Grid item xs={12} sm={4} lg={3}>
                        <Typography align="left" className={classes.title} variant="body1" id="tableTitle" component="div">
                            {props.title}
                        </Typography>
                    </Grid>
                    <Grid item xs={12} sm={8} lg={9} className={classes.actionButtons}>
                        {props.actions}
                    </Grid>
                </Grid>
            </div>
            <TableContainer>
                <Table>
                    <TableHead>
                        <TableRow>
                            {
                                props.headerData.map(header => (
                                    <StyledTableCell
                                        padding='default'
                                    >
                                        {header.label}
                                        {sortable && header.sort && <TableSortLabel
                                            active={true}
                                            direction={orderBy === header.key ? order : 'asc'}
                                            onClick={createSortHandler(header.key)}
                                        />
                                        }
                                    </StyledTableCell>
                                ))
                            }
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {props.tableData && props.tableData.length > 0 ? <RenderTableData /> : 'No data provided'}
                    </TableBody>
                </Table>
            </TableContainer>
        </Paper>
    )
}

export default CustomDataTable