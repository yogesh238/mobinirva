import React from 'react';
import { Typography, Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import {
  InputLabel,
  MenuItem,
  FormControl,
  Select,
  Link,
  Card, 
  Divider
} from '@material-ui/core';

import Carousel, { slidesToShowPlugin } from '@brainhubeu/react-carousel';
import CustomImageCard from './CustomImageCard';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
      },
      paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
      },
      cover: {
        marginTop: theme.spacing(1),
        height: 80,
        minWidth: 100,
    },
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
    title: {
        flex: '1 2 80%',
        color: '#131111',
        fontSize: 22,
        fontWeight: 'bold',
        marginBottom: theme.spacing(2),
        padding: 25
    },
    image:{
        width: '100%',
    },
    ccTitle: {
        fontSize: 16,
        fontWeight: '500',
        color: '#000000',
    },
    // links: {
    //     '& > * + *': {
    //       marginLeft: theme.spacing(2),
    //     },
    //   },
      controversies:{
          padding: '20px 30px 3px 30px',
        //   borderBottom: '3px solid #F6F6F6',
      }

}));


export const tableHeaders = [
    {
        label: '',
        key: 'label',
        sort: false
    },
    {
        label: '2017',
        key: '2017',
        sort: false
    },
    {
        label: '2018',
        key: '2018',
        sort: false
    },
    {
        label: '2019',
        key: '2019',
        sort: false
    },
    {
        label: '2020',
        key: '2020',
        sort: false
    },
    {
        label: '2021',
        key: '2021',
        sort: false
    }
]


export const emissionTableData =[
    {
        label:'Total Emission (Million tonnes)',
        '2017':'120',
        '2018':'100',
        '2019':'70',
        '2020':'22',
        '2021':'20',
    },
    
]

export const emissionGraphData = [
    {
        name: '2017',
        value: 22.4
    },
    {
        name: '2018',
        value: 21
    },
    {
        name: '2019',
        value: 19
    },
    {
        name: '2020',
        value: 18
    },
    {
        name: '2021',
        value: 15
    }
]

export const revenueGraphData = [
    {
        name: '2017',
        value: 0.90
    },
    {
        name: '2018',
        value: 0.89
    },
    {
        name: '2019',
        value: 0.88
    },
    {
        name: '2020',
        value: 0.92
    },
    {
        name: '2021',
        value: 0.88
    }
]


export const corouselData = [{
    imgURL: 'https://images.unsplash.com/photo-1560977490-bf4117146fc9?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1229&q=80',
    imgHeight: 250,
    imgAlt: 'Background Image',
    title: `Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging across all continents except Antarctica`
}, {
    imgURL: 'https://images.unsplash.com/photo-1560977490-bf4117146fc9?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1229&q=80',
    imgHeight: 250,
    imgAlt: 'Background Image',
    title: `Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging across all continents except Antarctica`
}, {
    imgURL: 'https://images.unsplash.com/photo-1560977490-bf4117146fc9?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1229&q=80',
    imgHeight: 250,
    imgAlt: 'Background Image',
    title: `Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging across all continents except Antarctica`
}, {
    imgURL: 'https://images.unsplash.com/photo-1560977490-bf4117146fc9?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1229&q=80',
    imgHeight: 250,
    imgAlt: 'Background Image',
    title: `Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging across all continents except Antarctica`
}]



export const controversies = [{
    imgURL: 'https://images.unsplash.com/photo-1560977490-bf4117146fc9?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1229&q=80',
    imgHeight: 250,
    imgAlt: 'Background Image',
    title: `Premature factory re-opening leads to controversy`,
    description: `Company ABC involved in controversy with authorities over reopening its faulty healt concerns.`,
    list_actions :['ABC', 'Human Capital','Medium', 'Negative']
}, {
    imgURL: 'https://images.unsplash.com/photo-1560977490-bf4117146fc9?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1229&q=80',
    imgHeight: 250,
    imgAlt: 'Background Image',
    title: `Major Auto Company recalls model Y`,
    description: `Company XYZ recalls 500,000 units of its model Y to address faulty steering column`,
    list_actions :['ABC', 'Governance','Medium', 'Negative']
}, {
    imgURL: 'https://images.unsplash.com/photo-1560977490-bf4117146fc9?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1229&q=80',
    imgHeight: 250,
    imgAlt: 'Background Image',
    title: `Environment liability provisions on the rise`,
    description: `Company MNC sets aside $8 million as reserves for environment liabilities in FY20`,
    list_actions :['MNC', 'Environment','High', 'Negative']
}, {
    imgURL: 'https://images.unsplash.com/photo-1560977490-bf4117146fc9?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1229&q=80',
    imgHeight: 250,
    imgAlt: 'Background Image',
    title: `Negotiation with unions hits a stalemate`,
    description: `Company RY reports work stoppage in its plant due to unfavourable negotiation outcomes with its employee unions`,
    list_actions :['RY', 'Social Capital','Medium', 'Negative']

}, {
    imgURL: 'https://images.unsplash.com/photo-1560977490-bf4117146fc9?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1229&q=80',
    imgHeight: 250,
    imgAlt: 'Background Image',
    title: `NEV capex explodes`,
    description: `Company BMG to maintain high capex spending on New Energy Vehicles (NEV) technology that will enable it to expand on its NEV credits`,
    list_actions :['BMG', 'Environment','Medium', 'Positive']

}]

export const ghgCompanyDetails = 'Company A explores for and produces crude oil and natural gas in the united States and internationally. It operated through Upstream, Downstream, and Chemical segments. The company is also involved in the manufacture, trade, transport, and sale of crude oil, natural gas, petroleum products, petrochemicals, and other specialty products; and manufactures and sells petrochemicals, including olefins, polyolefins, aromatics, and various and other petrochemicals. The company was founded in 1870 and is based in lrving, Texas.'





function NirvaHomeIndustry(props) {
  const classes = useStyles();
  const [sortBy, setSortBy] = React.useState('materiality');
  const preventDefault = (event) => event.preventDefault();
  const handleChange = (event) => {
    setSortBy(event.target.value);
  };


  return (
      <Grid
        container
        direction="row"
        justify="center"
        alignItems="center"
        spacing={2}
        className={classes.row}
      >
      <Grid item xs={12}>
          <Typography variant="subtitle2" color='textSecondary'>
              Showing results for "controversies for companies in auto industry since 2015" (1-76)
          </Typography>
      </Grid>

    
      <Grid item xs={12}>
          <Card
              className={classes.card}
              elevation={4}
              style={{padding:35}}
          >
        <Grid container style={{display: 'flex'}}>
            <Grid item xs={12} md={9}>
                <Typography align="left" className={classes.title} variant="body1" id="tableTitle" component="div">
                    Material controversies for companies in "auto industry" since 2015
                </Typography>
                
            </Grid>
            <Grid item xs={12} md={3} >
                <FormControl variant="outlined" className={classes.formControl}>
                    <InputLabel id="demo-simple-select-outlined-label">Sort By</InputLabel>
                    <Select
                    labelId="demo-simple-select-outlined-label"
                    id="demo-simple-select-outlined"
                    value={sortBy}
                    onChange={handleChange}
                    label="Sort by"
                    >
                    <MenuItem value="materiality">
                        <em>Materiality</em>
                    </MenuItem>
                    <MenuItem value="recent">Recent</MenuItem>
                    <MenuItem value="older">Older</MenuItem>
                    <MenuItem value="mostViewed">Most Viewed</MenuItem>
                    </Select>
                </FormControl>
            </Grid>
        </Grid>



        {controversies && controversies.map((item, index) => (
            
            <Grid container spacing={3} className={classes.controversies} key={index}>
                <Grid item sx={6} md={2}>
                    <img className={classes.image} alt={item.imgAlt} src={item.imgURL} />
                </Grid>
                <Grid item sx={6} md={10}>
                    <Typography variant="h5" component="h2">
                    {item.title}
                    </Typography>
                    <Typography variant="p" className={classes.ccTitle} style={{paddingTop: 5}}>
                    {item.description}
                    </Typography>
                    <Grid container justify="space-between" style={{padding: '20px'}} >
                        {item.list_actions.map((item, index) => (
                            <Link href="#" onClick={preventDefault}>
                                {item}
                                <Divider orientation="vertical"  />
                            </Link>
                        ))}
        
                        
                    </Grid>

                  


                </Grid>
            <Divider  />

            </Grid>


                ))}







          </Card>
      </Grid>




      <Grid item xs={12}>
          <Typography variant="p" className={classes.title} >
              Do You Know?
          </Typography>
          <Carousel
              plugins={[
                  {
                      resolve: slidesToShowPlugin,
                      options: {
                          numberOfSlides: 3
                      }
                  },
              ]}
          >
              {corouselData && corouselData.map((item, index) => (
                  <CustomImageCard
                      key={index}
                      imgURL={item.imgURL}
                      imgAlt={item.title}
                  />
              ))}
          </Carousel>
      </Grid>
  </Grid>
    );
  }
  
  export default NirvaHomeIndustry;