import React from 'react';
// import PropTypes from 'prop-types';

import { makeStyles } from '@material-ui/core/styles';
import { FormControlLabel, Checkbox } from '@material-ui/core';
// import { CheckBox, CheckBoxOutlineBlank, } from '@material-ui/icons';

import CustomTreeView from '../../components/TreeView';

const buildComponent = ({ key, id, lable, isChecked, isFull, onClick, value }) => (
    <div key={key} id={key}  >
        <FormControlLabel
            control={
                <Checkbox
                    checked={isChecked}
                    onChange={(event) => onClick ? onClick(event, value) : null}
                    // name="checkedB"
                    size='small'
                    color={isFull ? 'primary' : 'default'}
                />
            }
            label={lable}
        />
        {/* {isChecked ?
            isFull ?
                <CheckBox htmlColor={'blue'} />
                :
                <CheckBox htmlColor={'grey'} />
            :
            <CheckBoxOutlineBlank htmlColor={'grey'} />
        }
        <span>{lable || ''}</span> */}
    </div>
)

const useStyles = makeStyles({
    root: {
        height: 110,
        flexGrow: 1,
        maxWidth: 400,
    },
});

const CompanyBySectorTree = (props) => {
    const classes = useStyles();

    const [dataForTree, setDataForTree] = React.useState([])
    const [defaultExpandNodes, setDefaultExpandNodes] = React.useState([])

    const makeData = (items, selected, companyClick, onParentClick, searchedCompany) => {
        // console.log("it is searched company", searchedCompany);
        if (items.length === 0 && !Array.isArray(items)) {
            return [];
        }
        return items.map((sector, i) => {
            let continentCompanies = [];
            let sectr = {
                id: `sectr-${i}-${sector.sectorid}`,
                children: [],
            }
            sectr.children = sector.industries.map((industry, j) => {
                let ind = {
                    id: `ind-${j}-${industry.industryid}`,
                    children: [],
                }
                if (industry && industry.companies && industry.companies.length) {
                    continentCompanies = continentCompanies.concat(industry.companies)
                    ind.children = industry.companies.map((comp, l) => {
                        // let check = selected && selected.length && selected.includes(comp.companyid) ? true : false;
                        let check = selected && selected.length && selected.filter(company => company.companyid === comp.companyid).length > 0 ? true : false;
                        if (comp.companyid === searchedCompany.companyid) {
                            setDefaultExpandNodes([sectr.id, ind.id, `comp-${l}-${comp.companyid}`])
                        }
                        return {
                            id: `comp-${l}-${comp.companyid}`,
                            name: buildComponent({
                                id: comp.companyid,
                                key: `comp-${l}-${comp.companyid}`,
                                isChecked: check,
                                isFull: true,
                                lable: comp.companyname,
                                onClick: companyClick,
                                value: comp
                            }),
                            ischecked: check,
                            children: [],
                        }
                    })
                }
                //checking any children of industry checked or not
                let industryChecked = ind.children && ind.children.length && ind.children.some(child => child.ischecked);
                ind.ischecked = industryChecked;

                let everyCompanyChecked = ind.children && ind.children.length && ind.children.every(child => child.ischecked)
                ind.isFull = everyCompanyChecked;

                //adding industry div to tree
                ind.name = buildComponent({
                    id: industry.industryid,
                    key: `ind-${j}-${industry.industryid}`,
                    isChecked: industryChecked,
                    isFull: everyCompanyChecked,
                    lable: industry.industry,
                    onClick: onParentClick,
                    value: industry.companies && industry.companies.length ? industry.companies : []
                })
                return ind;
            })

            //checking any children of sector checked or not
            let sectorChecked = sectr.children && sectr.children.length && sectr.children.some(child => child.ischecked);
            sectr.ischecked = sectorChecked;

            let everyIndustryChecked = sectr.children && sectr.children.length && sectr.children.every(child => child.isFull);
            sectr.isFull = everyIndustryChecked;

            //adding sector div to tree
            sectr.name = buildComponent({
                id: sector.sectorid,
                key: `sectr-${i}-${sector.sectorid}`,
                isChecked: sectorChecked,
                isFull: everyIndustryChecked,
                lable: sector.sector,
                onClick: onParentClick,
                value: continentCompanies
            })
            return sectr;
        });
    }

    const onNodeToggle = (event, nodes) => {
        // console.log("these are nodes", nodes);
        setDefaultExpandNodes(nodes)
    }

    React.useEffect(() => {
        let forTree = makeData(props.data, props.selectedCompanies, props.onCompanyClick, props.onParentClick, props.searchedCompany)
        setDataForTree(forTree)
    }, [props.selectedCompanies, props.searchedCompany])

    return (
        <CustomTreeView items={dataForTree} defaultExpandNodes={defaultExpandNodes} onNodeToggle={onNodeToggle} />
    )
}

export default CompanyBySectorTree;