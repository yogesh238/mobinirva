import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
    Paper, Grid, Typography
} from '@material-ui/core';
import RadioButtonCheckedIcon from '@material-ui/icons/RadioButtonChecked';
import loginImage from '../../assets/login.jpg'
import {AmplifyAuthenticator,  AmplifySignUp, AmplifySignIn} from "@aws-amplify/ui-react";

const useStyles = makeStyles((theme) => ({
    root: {
        height: '100vh',
    },
    image: {
        backgroundImage: `url(${loginImage})`,
        backgroundRepeat: 'no-repeat',
        backgroundColor:
            theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
        backgroundSize: 'cover',
        backgroundPosition: 'center',
    },
    paper: {
        display: 'flex',
        width: '100%',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    forgotPassword: {
        marginTop: theme.spacing(3),
    },
    login: {
        margin: theme.spacing(3, 0, 2),
    },
    signup: {
        margin: theme.spacing(3),
    },
    loginContainer: {
        // display: 'flex',
        // flexDirection: 'column',
        // margin: theme.spacing(6, 4 ),
    },
    nirvaFormContainer: {
        display: 'flex',
        flexGrow: 2,
        alignItems: 'center',
        paddingLeft: theme.spacing(5),
    },
    nirvaTitleContainer: {
        display: 'flex',
        flexGrow: 1,
        alignItems: 'center',
        padding: theme.spacing(5),
    },
    nirvaTitle: {
        fontSize: 34,
        fontWeight: 'bold',
        fontFamily: 'Roboto',
        marginLeft: 5,
    },
    icon: {
        cursor: 'pointer'
    }
}));

export default function Login(props) {
    const classes = useStyles();
    return (
        <Grid container component="main" className={classes.root}>
            <Grid item xs={false} sm={4} md={7} className={classes.image} />
            <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square >
                <Grid item xs={12} className={classes.nirvaTitleContainer} >
                    <RadioButtonCheckedIcon color='primary' fontSize='large' className={classes.icon} />
                    <Typography  className={classes.nirvaTitle}>NIRVA</Typography>
                </Grid>
                <Grid item xs={12} className={classes.nirvaFormContainer}>
             
                <AmplifyAuthenticator>
        <AmplifySignUp
            slot="sign-up"
            usernameAlias="email"
            formFields={[
                {
                    type: "name",
                    label: "Name",
                    placeholder: "Name",
                    inputProps: { required: true},
    
                },
            {
                type: "email",
                label: "Email",
                placeholder: "Email",
                inputProps: { required: true, autocomplete: "username" },
            },
            {
                type: "password",
                label: "Password",
                placeholder: "Password",
                inputProps: { required: true, autocomplete: "new-password" },
            },
            {
                type: "phone_number",
                label: "Phone",
                placeholder: "Phone",
                inputProps: { required: true},

            },

            ]} 
        />
         <AmplifySignIn slot="sign-in" usernameAlias="email" />
        </AmplifyAuthenticator>

                </Grid>
            </Grid>
        </Grid>
    );
}