
export const data = [
    {
        companyid: 2,
        companyname: "WOOLWORTHS GROUP LTD",
        environment: 22,
        socialcapital: 12,
        humancapital: 33,
        businessmodel: 10,
        leadership: 15,
        total: 52.15
    },
    {
        companyid: 11,
        companyname: "COLES GROUP LTD",
        environment: 33,
        socialcapital: 21,
        humancapital: 12,
        businessmodel: 19,
        leadership: 22,
        total: 45.05
    },
    {
        companyid: 12,
        companyname: "Avenue Supermarts Ltd",
        environment: 15,
        socialcapital: 32,
        humancapital: 22,
        businessmodel: 22,
        leadership: 11,
        total: 38.00
    },
    {
        companyid: 30,
        companyname: "Nestle India Ltd",
        environment: 26,
        socialcapital: 11,
        humancapital: 36,
        businessmodel: 15,
        leadership: 20,
        total: 27.50
    },
    {
        companyid: 39,
        companyname: "Hindustan Unilever Ltd",
        environment: 13,
        socialcapital: 36,
        humancapital: 29,
        businessmodel: 11,
        leadership: 10,
        total: 44.21
    },
    {
        companyid: 59,
        companyname: "ITC Ltd",
        environment: 52,
        socialcapital: 21,
        humancapital: 36,
        businessmodel: 15,
        leadership: 20,
        total: 29.50
    }
]

export const tableHeaders = [
    {
        label: '',
        key: 'companyname',
        sort: false
    },
    {
        label: 'Environment',
        key: 'environment',
        sort: true
    },
    {
        label: 'Social Capital',
        key: 'socialcapital',
        sort: true
    },
    {
        label: 'Human Capital',
        key: 'humancapital',
        sort: true
    },
    {
        label: 'Business Model & Innovation',
        key: 'businessmodel',
        sort: true
    },
    {
        label: 'Leadership & Governance',
        key: 'leadership',
        sort: true
    },
    {
        label: 'Total',
        key: 'total',
        sort: true
    },
    {
        label: 'Actions',
        key: 'action'
    },
]