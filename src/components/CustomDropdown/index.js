import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { PinDropSharp } from '@material-ui/icons';

const useStyles = makeStyles((theme) => ({
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
        color: 'blue'
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
}));

export default function CustomDropdown(props) {
    const classes = useStyles();

    return (
        <div>
            <FormControl variant="filled" className={classes.formControl}>
                <InputLabel id="custom-select-filled-label">{props.label}</InputLabel>
                <Select
                    labelId="custom-select-filled-label"
                    id="custom-select-filled"
                    autoWidth={true}
                    value={props.value}
                    onChange={props.onChange}
                >
                    {props.button ? props.button : null}
                    {props.options && props.options.length && props.options.map(option => (
                        <MenuItem value={option.value}>{option.label}</MenuItem>
                    ))}
                </Select>
                {/* <FormHelperText>Portfolio</FormHelperText> */}
            </FormControl>
        </div>
    );
}