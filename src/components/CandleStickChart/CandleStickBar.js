import React from 'react'
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    container: {
        display: 'flex',
        alignItems: 'center',
        // justifyContent:'center',
        // margin: 2,
        // backgroundColor: 'yellow'
    },
    barLabel: {
        margin: '0px 5px',
        // alignItems: 'center',
        // justifyContent:'center',
        // backgroundColor: 'green'
    }
}))

function CandleStickBar({ width = 300, height = 50, min, max, position }) {
    const classes = useStyles();
    const LOWER_LIMIT = -5;
    const UPPER_LIMIT = 5;
    const RANGE = UPPER_LIMIT - (LOWER_LIMIT);
    const WIDTH = width;
    const HEIGHT = height;
    const Y_MID = HEIGHT / 2; // vertically center value;
    const LINE_COLOR = '#3BC509';
    const TEXT_COLOR = '#292828';

    const CIRCLE_RADIUS = 5; // 5px
    /* left side Circle */
    const C_LEFT_X = CIRCLE_RADIUS; // left circle center position value
    const C_LEFT_COLOR = '#DF1010';

    /* right side Circle */
    const C_RIGHT_X = WIDTH - CIRCLE_RADIUS; // right circle center position value
    const C_RIGHT_COLOR = '#10A241';

    const MIN_POINT = (min - (LOWER_LIMIT)) * (WIDTH / RANGE);
    const MAX_POINT = (max - (LOWER_LIMIT)) * (WIDTH / RANGE);

    /* Rectangle */
    const RECT_X = MIN_POINT; // starting point 
    const RECT_WIDTH = MAX_POINT - RECT_X; // end point
    const RECT_Y = Y_MID - 8;
    const RECT_HEIGHT = 16;
    const RECT_COLOR = '#1BB1EC'

    /* DIMOND SHAPE */
    const POSITION_POINT = (position - (LOWER_LIMIT)) * (WIDTH / RANGE);
    const SIDE_LEN = 4;

    // console.log("CandleStickBar => ", {
    //     RECT_X, RECT_WIDTH, RECT_Y, RECT_HEIGHT, RANGE, width, min, max, position, height,
    //     "test": min - (LOWER_LIMIT) * (WIDTH / RANGE)
    // })

    return (
        <div className={classes.container}>
            <div className={classes.barLabel}>{LOWER_LIMIT}</div>
            <svg viewBox={`0 0 ${WIDTH} ${HEIGHT}`} width={WIDTH} height={HEIGHT} xmlns="http://www.w3.org/2000/svg">
                <rect x="0" y={Y_MID} rx="2" ry="2" width={WIDTH} height="1" fill={LINE_COLOR} />
                <circle r={CIRCLE_RADIUS} cx={C_LEFT_X} cy={Y_MID} fill={C_LEFT_COLOR} />
                <circle r={CIRCLE_RADIUS} cx={C_RIGHT_X} cy={Y_MID} fill={C_RIGHT_COLOR} />
                <rect x={RECT_X} y={RECT_Y} rx="2" ry="2" width={RECT_WIDTH} height={RECT_HEIGHT} fill={RECT_COLOR} />
                <polygon points={`${POSITION_POINT - SIDE_LEN},${Y_MID} ${POSITION_POINT},${Y_MID + 8} ${POSITION_POINT + SIDE_LEN},${Y_MID} ${POSITION_POINT},${HEIGHT / 2 - 8}`} fill="#0A67D4" />
                <g>
                    <text x={MIN_POINT} y={Y_MID - 15} font-family="Verdana" font-size="10" fill={TEXT_COLOR}>{min}</text>
                    <text x={MAX_POINT - 5} y={Y_MID - 15} font-family="Verdana" font-size="10" fill={TEXT_COLOR}>{max}</text>
                </g>
            </svg>
            <div className={classes.barLabel}>{UPPER_LIMIT}</div>
        </div>
    )
}

CandleStickBar.propTypes = {
    min: PropTypes.number,
    max: PropTypes.number,
    position: PropTypes.number,
    width: PropTypes.oneOfType([
        // PropTypes.string,
        PropTypes.number,
    ]),
    height: PropTypes.oneOfType([
        // PropTypes.string,
        PropTypes.number,
    ]),
}

export default CandleStickBar