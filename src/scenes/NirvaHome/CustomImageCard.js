import React from 'react';
import { Card, CardActionArea, CardMedia, CardContent, Typography, makeStyles } from '@material-ui/core';


const useStyles = makeStyles((theme) => ({
    card: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 1,
        margin: 5,
        marginRight: 30,
        // minWidth: '33%',
        // borderRadius: 1,
        // backgroundSize:'cover',
        // height: 100,
        // backgroundColor:'yellow',
    },
    imgStyles: {
        backgroundSize: 'cover',
        // backgroundColor:'yellow',
    },

    ccTitle: {
        fontSize: 16,
        fontWeight: '500',
        color: '#000000',
    },
}));

const CustomImageCard = ({
    imgURL = 'https://images.unsplash.com/photo-1560977490-bf4117146fc9?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1229&q=80',
    imgHeight = 240,
    imgAlt = 'Background Image',
    title = `Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging across all continents except Antarctica`
}) => {
    const classes = useStyles();

    return (
        <Card
            className={classes.card}
            elevation={4}
        >
            <CardActionArea>
                <CardMedia
                    component="img"
                    alt={imgAlt}
                    height={imgHeight}
                    image={imgURL}
                    title={imgAlt}
                    className={classes.imgStyles}
                />
                <CardContent>
                    <Typography className={classes.ccTitle} component="p">
                        {title}
                    </Typography>
                </CardContent>
            </CardActionArea>
        </Card>
    )
};

export default CustomImageCard;