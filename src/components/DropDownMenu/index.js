import React from 'react';
import {
    Button,
    // IconButton,
    Menu, MenuItem
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles'

// import MoreVertIcon from '@material-ui/icons/MoreVert';
import {
    ExpandMore as ExpandMoreIcon,
    ExpandLess as ExpandLessIcon,
} from '@material-ui/icons';

const useStyles = makeStyles((theme) => ({
    dropDownBtn: {
        textTransform: 'none',
        fontSize: 14,
        marginLeft: 5,
        minWidth: 120,
        paddingLeft: 10,
        paddingRight: 10,
        justifyContent: 'space-between',
    }
}))

export default function CustomMenu({ title, value, color = 'primary', size = 'large', btnStyles = {}, variant = 'contained',
    options, onClick, onChange, ...props }) {
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = React.useState(null);
    const open = Boolean(anchorEl);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
        if (typeof onClick === 'function') {
            onClick(event);
        }
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const handleItemClick = (item) => {
        setAnchorEl(null);
        if (item.value && typeof onChange === 'function') {
            onChange(item);
        }
    };

    let endIcon = open ? <ExpandLessIcon /> : <ExpandMoreIcon />;

    // const ITEM_HEIGHT = 48;
    const ITEM_HEIGHT = 100;
    const ITEM_PADDING_TOP = 10;
    return (
        <div>
            <Button
                aria-label="menu-button"
                aria-controls="customized-menu"
                aria-haspopup="true"
                variant={variant}
                size={size}
                color={color}
                onClick={handleClick}
                endIcon={endIcon}
                className={classes.dropDownBtn}
                style={btnStyles}
            >
                {value ? value : title ? title : 'Menu Button'}
            </Button>

            <Menu
                id="long-menu"
                anchorEl={anchorEl}
                keepMounted
                open={open}
                onClose={handleClose}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'left',
                }}
               
                getContentAnchorEl={null}
                PaperProps={{
                    style: {
                        // maxHeight: ITEM_HEIGHT * 6.5,
                        // maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
                        minWidth: '15ch',
                    },
                }}

            // MenuProps={MenuProps}
            >
                {props.button ? props.button : null}
                {options && Array.isArray(options) && options.map((option) => (
                    <MenuItem key={option.id}
                        onClick={() => handleItemClick(option)}>
                        {option.label}
                    </MenuItem>
                ))}
            </Menu>
        </div >
    );
}