
function genId() {
    /* Generate a random string (unique id?)  */
    return Math.random().toString(36).slice(2)
}

const truncStr = (string, length = 20) => {
    /* Truncate string at the end
    console.log(
    truncStr('Hi, I should be truncated because I am too loooong!', 36),
    );
    // Hi, I should be truncated because...
    */
    const chars = Array.from(string)
    return chars.length < length ? string : `${chars.slice(0, length - 3).join('')}...`;
    // return string.length < length ? string : `${string.slice(0, length - 3)}...`;
};

export {
    genId,
    truncStr,
}