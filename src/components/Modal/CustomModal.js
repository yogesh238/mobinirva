import React from 'react';
import PropTypes from 'prop-types';

import CloseIcon from '@material-ui/icons/Close';
import { withStyles } from '@material-ui/core/styles';
import { Button, Dialog, IconButton, Typography } from '@material-ui/core';

import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';

const styles = (theme) => ({
    root: {
        margin: 0,
        padding: theme.spacing(2),
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing(1),
        top: theme.spacing(1),
        color: theme.palette.grey[500],
    },
});

const DialogTitle = withStyles(styles)((props) => {
    const { children, classes, onClose, ...other } = props;
    return (
        <MuiDialogTitle disableTypography className={classes.root} {...other}>
            <Typography variant="h6">{children}</Typography>
            {onClose ? (
                <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
                    <CloseIcon />
                </IconButton>
            ) : null}
        </MuiDialogTitle>
    );
});

const DialogContent = withStyles((theme) => ({
    root: {
        padding: theme.spacing(2),
    },
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
    root: {
        margin: 0,
        padding: theme.spacing(1),
    },
}))(MuiDialogActions);

const CustomWidthDialog = ({
    maxWidth = 'sm', open = false, onClose, onAdd, children,
    title = 'Title', ...props
}) => {
    const [modalOpen, setModalOpen] = React.useState(open);

    React.useEffect(() => {
        setModalOpen(open)
    }, [open])

    const handleClose = () => {
        if (typeof onClose === 'function' && onClose != null) {
            onClose();
        } else {
            setModalOpen(false);
        }
    };

    const handleAdd = () => {
        if (typeof onAdd === 'function' && onAdd != null) {
            onAdd();
        } else {
            setModalOpen(false);
        }
    }

    return (
        <Dialog
            fullWidth={true}
            fullScreen={false}
            // scroll={'body'}
            maxWidth={maxWidth}
            open={modalOpen}
            onClose={handleClose}
            aria-labelledby="max-width-dialog-title"
        >
            <DialogTitle id="customized-dialog-title" onClose={handleClose}>
                {title}
            </DialogTitle>

            <DialogContent dividers>{children}</DialogContent>
            <DialogActions>
                <Button variant="contained" onClick={handleClose}  >
                    clear
             </Button>
                <Button variant="contained" autoFocus onClick={handleAdd} color="primary">
                    add
             </Button>
            </DialogActions>
        </Dialog>
    );
}

CustomWidthDialog.propTypes = {
    maxWidth: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.bool,
    ]),
    open: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    children: PropTypes.element,
};

export default CustomWidthDialog;