import { Typography, Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import {
    Card, 
    CardContent
} from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
      },
      paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
      },
      cover: {
        marginTop: theme.spacing(1),
        height: 80,
        minWidth: 100,
    },

}));

export const tableHeaders = [
    {
        label: '',
        key: 'label',
        sort: false
    },
    {
        label: '2017',
        key: '2017',
        sort: false
    },
    {
        label: '2018',
        key: '2018',
        sort: false
    },
    {
        label: '2019',
        key: '2019',
        sort: false
    },
    {
        label: '2020',
        key: '2020',
        sort: false
    },
    {
        label: '2021',
        key: '2021',
        sort: false
    }
]


export const emissionTableData =[
    {
        label:'Total Emission (Million tonnes)',
        '2017':'120',
        '2018':'100',
        '2019':'70',
        '2020':'22',
        '2021':'20',
    },
    
]

export const emissionGraphData = [
    {
        name: '2017',
        value: 22.4
    },
    {
        name: '2018',
        value: 21
    },
    {
        name: '2019',
        value: 19
    },
    {
        name: '2020',
        value: 18
    },
    {
        name: '2021',
        value: 15
    }
]

export const revenueGraphData = [
    {
        name: '2017',
        value: 0.90
    },
    {
        name: '2018',
        value: 0.89
    },
    {
        name: '2019',
        value: 0.88
    },
    {
        name: '2020',
        value: 0.92
    },
    {
        name: '2021',
        value: 0.88
    }
]

export const corouselData = [{
    imgURL: 'https://images.unsplash.com/photo-1560977490-bf4117146fc9?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1229&q=80',
    imgHeight: 250,
    imgAlt: 'Background Image',
    title: `Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging across all continents except Antarctica`
}, {
    imgURL: 'https://images.unsplash.com/photo-1560977490-bf4117146fc9?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1229&q=80',
    imgHeight: 250,
    imgAlt: 'Background Image',
    title: `Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging across all continents except Antarctica`
}, {
    imgURL: 'https://images.unsplash.com/photo-1560977490-bf4117146fc9?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1229&q=80',
    imgHeight: 250,
    imgAlt: 'Background Image',
    title: `Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging across all continents except Antarctica`
}, {
    imgURL: 'https://images.unsplash.com/photo-1560977490-bf4117146fc9?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1229&q=80',
    imgHeight: 250,
    imgAlt: 'Background Image',
    title: `Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging across all continents except Antarctica`
}]

export const ghgCompanyDetails = 'Company A have a holistic sourcing policy for all our supplier tiers and have a detailed sourcing policy document to guide our procurement team'



function NirvaHomeStrategy(props) {
    const classes = useStyles()

    return (
        <Grid
        container
        direction="row"
        justify="center"
        alignItems="center"
        spacing={2}
        className={classes.row}
    // xs={10}
    >
        <Grid item xs={12}>
            <Typography variant="subtitle2" color='textSecondary'>
                Showing results for "sustainable sourcing policy of company A"
            </Typography>
        </Grid>
      
        <Grid item xs={12}>
            <Card className={classes.card} elevation={4}>
                <CardContent >
                    <Typography component="p" variant="p" className={classes.title}>
                        Sustainable sourcing policy of Company A
                    </Typography>
                    <Typography
                        component="p">
                            {ghgCompanyDetails}
                    </Typography>
                </CardContent>
            </Card>
        </Grid>
        <Grid item xs={12}>
            <Card className={classes.card} elevation={4} >
                <CardContent>
                    <Typography component="p" variant="p" className={classes.title}>
                        Nirva ESG rating
                    </Typography>
                    <Typography
                        component="p">
                            Score for sustainable sourcing - 4.3
                    </Typography>
                </CardContent>
                
            </Card>
        </Grid>

        <Grid item xs={12}>
            <Card className={classes.card} elevation={4} >
                <CardContent>
                    <Typography component="p" variant="p" className={classes.title}>
                        Sustainable sourcing policy
                    </Typography>
                    <Typography
                        component="p">
                            Company sourcing policy document 2020
                    </Typography>
                </CardContent>
                
            </Card>
        </Grid>

        <Grid item xs={12}>
        <Typography variant="p" className={classes.title} >
                More strategy reports
            </Typography>
        </Grid>


        <Grid container spacing={6}>
            
            <Grid item xs={4}>
            <Card className={classes.card} elevation={4}>
                <CardContent >
                    <Typography component="p" className={classes.title}>
                        Strategy reports and sustainability policy of company B
                    </Typography>
                </CardContent>
            </Card>
        </Grid>
        <Grid item xs={4}>
            <Card className={classes.card} elevation={4}>
                <CardContent >
                    <Typography component="p" className={classes.title}>
                        Strategy reports and sustainability policy of company C
                    </Typography>
                </CardContent>
            </Card>
        </Grid>
        <Grid item xs={4}>
            <Card className={classes.card} elevation={4}>
                <CardContent >
                    <Typography component="p" className={classes.title}>
                        Strategy reports and sustainability policy of company D
                    </Typography>
                </CardContent>
            </Card>
        </Grid>
        </Grid>
    </Grid>
    );
  }
  
  export default NirvaHomeStrategy;