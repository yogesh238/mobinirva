export const DATA = [
    {
        portfolioid: 1,
        portfolioname: 'Portfolio A',
        mandateid: 'm1',
        mandatename: 'Mandate A',
        companies: [
            {
                companyid: 1,
                companyname: "company A"

            },
            {
                companyid: 2,
                companyname: "company B"

            },
            {
                companyid: 3,
                companyname: "company C"

            },
        ]
    },
    {
        portfolioid: 2,
        portfolioname: 'Portfolio B',
        mandateid: 'm2',
        mandatename: 'Mandate B',
        companies: [
            {
                companyid: 1,
                companyname: "company A"

            },
            {
                companyid: 2,
                companyname: "company B"

            },
            {
                companyid: 3,
                companyname: "company C"

            },
        ]
    },
    {
        portfolioid: 3,
        portfolioname: 'Portfolio C',
        mandateid: 'm3',
        mandatename: 'Mandate C',
        companies: [
            {
                companyid: 1,
                companyname: "company A"

            },
            {
                companyid: 2,
                companyname: "company B"

            },
            {
                companyid: 3,
                companyname: "company C"

            },
            {
                companyid: 4,
                companyname: "company D"

            },
        ]
    },
]

export const MANDATEDATA = [
    {
        mandateid: 'm1',
        mandatename: 'Mandate A',
        assosiatedtaxonomyid: 'nirva',
        assosiatedtaxonomyname: 'Nirva Dynamic',
        'esg_dimension': [
            {
                id: 1,
                key: 'enviroment',
                shortname: 'Environment',
                name: 'Environment',
                value: 65,
                categories: [
                    {
                        id: 1,
                        key: "afford",
                        name: 'Access and Affordabillity',
                        value: '10',
                    },
                    {
                        id: 2,
                        key: "safety",
                        name: 'Product Quality & Safety',
                        value: '15',
                    },
                    {
                        id: 3,
                        key: "welfare",
                        name: 'Customer Welfare',
                        value: '5',
                    },
                    {
                        id: 4,
                        key: "selling",
                        name: 'Selling Practices & Product Labelling',
                        value: '25',
                    },
                    {
                        id: 5,
                        key: "labor",
                        name: 'Labor Practices',
                        value: '10',
                    },
                ]
            },
            {
                id: 2,
                key: 'socialcapital',
                shortname: 'Social Capital',
                name: 'Social Capital',
                value: 0,
                categories:[]
            },
            {
                id: 3,
                key: 'humancapital',
                shortname: 'Human Capital',
                name: 'Human Capital',
                value: 0,
                categories:[],
            },
            {
                id: 4,
                key: 'businessmodel',
                shortname: 'Business Model',
                name: 'Business Model & Innovation',
                value: 0,
                categories:[],
            },
            {
                id: 5,
                key: 'leadership',
                shortname: 'Leadership',
                name: 'Leadership & Governance',
                value: 0,
                categories:[],
            }
        ]
    },
    {
        mandateid: 'm2',
        mandatename: 'Mandate B',
        assosiatedtaxonomyid: 'nirva',
        assosiatedtaxonomyname: 'Nirva Dynamic',
    },
    {
        mandateid: 'm3',
        mandatename: 'Mandate C',
        assosiatedtaxonomyid: 'nirva',
        assosiatedtaxonomyname: 'Nirva Dynamic',
    },
    {
        mandateid: 'm4',
        mandatename: 'Mandate D',
        assosiatedtaxonomyid: 'nirva',
        assosiatedtaxonomyname: 'Nirva Dynamic',
    },
    {
        mandateid: 'm5',
        mandatename: 'Mandate E',
        assosiatedtaxonomyid: 'nirva',
        assosiatedtaxonomyname: 'Nirva Dynamic',
    },
    {
        mandateid: 'm6',
        mandatename: 'Mandate F',
        assosiatedtaxonomyid: 'nirva',
        assosiatedtaxonomyname: 'Nirva Dynamic',
    },
    {
        mandateid: 'm7',
        mandatename: 'Mandate G',
        assosiatedtaxonomyid: 'nirva',
        assosiatedtaxonomyname: 'Nirva Dynamic',
    },
    {
        mandateid: 'm8',
        mandatename: 'Mandate H',
        assosiatedtaxonomyid: 'nirva',
        assosiatedtaxonomyname: 'Nirva Dynamic',
    },
    {
        mandateid: 'm9',
        mandatename: 'Mandate I',
        assosiatedtaxonomyid: 'nirva',
        assosiatedtaxonomyname: 'Nirva Dynamic',
    },
    {
        mandateid: 'm10',
        mandatename: 'Mandate J',
        assosiatedtaxonomyid: 'nirva',
        assosiatedtaxonomyname: 'Nirva Dynamic',
    },
]

export const ESG_DATA = [
    {
        groupId: 1,
        groupName: 'Environment',
        categories: [
            {
                categoryId: 1,
                categoryName: 'Environment Catg-1'
            },
            {
                categoryId: 2,
                categoryName: 'Environment Catg-2'
            },
            {
                categoryId: 3,
                categoryName: 'Environment Catg-3'
            },
            {
                categoryId: 4,
                categoryName: 'Environment Catg-4'
            },
            {
                categoryId: 5,
                categoryName: 'Environment Catg-5'
            },
        ]
    },
    {
        groupId: 2,
        groupName: 'Social Capital',
        categories: [
            {
                categoryId: 1,
                categoryName: 'Access and Affordability'
            },
            {
                categoryId: 2,
                categoryName: 'Product Quality & Safety'
            },
            {
                categoryId: 3,
                categoryName: 'Customer Welfare'
            },
            {
                categoryId: 4,
                categoryName: 'Selling Practices & Product Labeling'
            },
            {
                categoryId: 5,
                categoryName: 'Labor Practices'
            },
        ]
    },
    {
        groupId: 3,
        groupName: 'Human Capital',
        categories: [
            {
                categoryId: 1,
                categoryName: 'Human Capital Catg-1'
            },
            {
                categoryId: 2,
                categoryName: 'Human Capital Catg-2'
            },
            {
                categoryId: 3,
                categoryName: 'Human Capital Catg-3'
            },
            {
                categoryId: 4,
                categoryName: 'Human Capital Catg-4'
            },
            {
                categoryId: 5,
                categoryName: 'Human Capital Catg-5'
            },
        ]
    },
    {
        groupId: 4,
        groupName: 'Business Modal & Innovation',
        categories: [
            {
                categoryId: 1,
                categoryName: 'Bmodal Catg-1'
            },
            {
                categoryId: 2,
                categoryName: 'Bmodal Catg-2'
            },
            {
                categoryId: 3,
                categoryName: 'Bmodal Catg-3'
            },
            {
                categoryId: 4,
                categoryName: 'Bmodal Catg-4'
            },
            {
                categoryId: 5,
                categoryName: 'Bmodal Catg-5'
            },
        ]
    },
    {
        groupId: 5,
        groupName: 'Leadership & Governance',
        categories: [
            {
                categoryId: 1,
                categoryName: 'Leadership Catg-1'
            },
            {
                categoryId: 2,
                categoryName: 'Leadership Catg-2'
            },
            {
                categoryId: 3,
                categoryName: 'Leadership Catg-3'
            },
            {
                categoryId: 4,
                categoryName: 'Leadership Catg-4'
            },
            {
                categoryId: 5,
                categoryName: 'Leadership Catg-5'
            },
        ]
    },
]

export const taxonomyData = [
    {
        taxonomyid: 'nirva',
        taxonomyname: 'Nirva Dynamic',
        'esg_dimension': [
            {
                id: 1,
                key: 'enviroment',
                shortname: 'Environment',
                name: 'Environment',
                value: '',
                categories: [
                    {
                        id: 1,
                        key: "afford",
                        name: 'Access and Affordabillity',
                        value: '',
                    },
                    {
                        id: 2,
                        key: "safety",
                        name: 'Product Quality & Safety',
                        value: '',
                    },
                    {
                        id: 3,
                        key: "welfare",
                        name: 'Customer Welfare',
                        value: '',
                    },
                    {
                        id: 4,
                        key: "selling",
                        name: 'Selling Practices & Product Labelling',
                        value: '',
                    },
                    {
                        id: 5,
                        key: "labor",
                        name: 'Labor Practices',
                        value: '',
                    },
                ]
            },
            {
                id: 2,
                key: 'socialcapital',
                shortname: 'Social Capital',
                name: 'Social Capital',
                value: '',
                categories: []
            },
            {
                id: 3,
                key: 'humancapital',
                shortname: 'Human Capital',
                name: 'Human Capital',
                value: '',
                categories: []
            },
            {
                id: 4,
                key: 'businessmodel',
                shortname: 'Business Model',
                name: 'Business Model & Innovation',
                value: '',
                categories: []
            },
            {
                id: 5,
                key: 'leadership',
                name: 'Leadership & Governance',
                value: '',
                categories: []
            }
        ]
    },
    {
        taxonomyid: 'sasb',
        taxonomyname: 'SASB',
        'esg_dimension': []
    },
    {
        taxonomyid: 'gri',
        taxonomyname: 'GRI',
        'esg_dimension': []
    }
]


const COMPANY_STATS = [
    {
        "sector": 'Environment',
        "min": 1,
        "max": 4,
        "position": 4,

    },
    {
        "sector": 'Social Capital',
        "min": -3,
        "max": 0,
        "position": -1,

    },
    {
        "sector": 'Human Capital',
        "min": -1,
        "max": 2,
        "position": -1,

    },
    {
        "sector": 'Business Model & Innovation',
        "min": -4,
        "max": -1,
        "position": -4,

    },
    {
        "sector": 'Leadership & Governance',
        "min": 0,
        "max": 3,
        "position": 2,

    },
]