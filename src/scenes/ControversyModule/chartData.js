export const COMPANY_STATS = [
    {
        "sector": 'Environment',
        "min": 1,
        "max": 4,
        "position": 4,

    },
    {
        "sector": 'Social Capital',
        "min": -3,
        "max": 0,
        "position": -1,

    },
    {
        "sector": 'Human Capital',
        "min": -1,
        "max": 2,
        "position": -1,

    },
    {
        "sector": 'Business Model & Innovation',
        "min": -4,
        "max": -1,
        "position": -4,

    },
    {
        "sector": 'Leadership & Governance',
        "min": 0,
        "max": 3,
        "position": 2,

    },
]