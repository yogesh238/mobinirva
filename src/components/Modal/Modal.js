import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import { Modal, Backdrop, Fade, } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({

}));

const CustomModal = ({ open = false, onClose, children }) => {
    const classes = useStyles();
    return (
        <Modal
            open={open}
            onClose={onClose}
            aria-labelledby="schedule interview"
            aria-describedby="schedule interview description"
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
                timeout: 500,
            }}
        >
            <Fade in={open}>

                <div className={classes.modalIntvShd} >
                    {children}
                </div>
            </Fade>
        </Modal>
    )
}

CustomModal.propTypes = {
    onClose: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired,
    // selectedValue: PropTypes.string.isRequired,
  };

export default CustomModal;
