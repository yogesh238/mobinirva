import { Typography, Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import {
    Button, 
    Card, 
    CardActions, 
    CardContent
} from '@material-ui/core';

import CustomDataTable from '../../components/CustomDataTable';

import c1 from '../../assets/c1.jpg'
import Carousel, { slidesToShowPlugin } from '@brainhubeu/react-carousel';

import CustomImageCard from './CustomImageCard';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    cover: {
        marginTop: theme.spacing(1),
        height: 80,
        minWidth: 100,
    },
    title: {
        flex: '1 2 80%',
        color: '#131111',
        fontSize: 22,
        fontWeight: 'bold',
        marginBottom: theme.spacing(2),
    },
    image:{
        width: '100%',
        padding: 20,
    }


}));

export const tableHeaders = [
    {
        label: '',
        key: 'label',
        sort: false
    },
    {
        label: '2017',
        key: '2017',
        sort: false
    },
    {
        label: '2018',
        key: '2018',
        sort: false
    },
    {
        label: '2019',
        key: '2019',
        sort: false
    },
    {
        label: '2020',
        key: '2020',
        sort: false
    },
    {
        label: '2021',
        key: '2021',
        sort: false
    }
]


export const emissionTableData =[
    {
        label:'Total Emission (Million tonnes)',
        '2017':'120',
        '2018':'100',
        '2019':'70',
        '2020':'22',
        '2021':'20',
    },
    
]

export const emissionGraphData = [
    {
        name: '2017',
        value: 22.4
    },
    {
        name: '2018',
        value: 21
    },
    {
        name: '2019',
        value: 19
    },
    {
        name: '2020',
        value: 18
    },
    {
        name: '2021',
        value: 15
    }
]

export const revenueGraphData = [
    {
        name: '2017',
        value: 0.90
    },
    {
        name: '2018',
        value: 0.89
    },
    {
        name: '2019',
        value: 0.88
    },
    {
        name: '2020',
        value: 0.92
    },
    {
        name: '2021',
        value: 0.88
    }
]

export const corouselData = [{
    imgURL: 'https://images.unsplash.com/photo-1560977490-bf4117146fc9?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1229&q=80',
    imgHeight: 250,
    imgAlt: 'Background Image',
    title: `Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging across all continents except Antarctica`
}, {
    imgURL: 'https://images.unsplash.com/photo-1560977490-bf4117146fc9?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1229&q=80',
    imgHeight: 250,
    imgAlt: 'Background Image',
    title: `Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging across all continents except Antarctica`
}, {
    imgURL: 'https://images.unsplash.com/photo-1560977490-bf4117146fc9?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1229&q=80',
    imgHeight: 250,
    imgAlt: 'Background Image',
    title: `Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging across all continents except Antarctica`
}, {
    imgURL: 'https://images.unsplash.com/photo-1560977490-bf4117146fc9?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1229&q=80',
    imgHeight: 250,
    imgAlt: 'Background Image',
    title: `Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging across all continents except Antarctica`
}]

export const ghgCompanyDetails = 'Company A explores for and produces crude oil and natural gas in the united States and internationally. It operated through Upstream, Downstream, and Chemical segments. The company is also involved in the manufacture, trade, transport, and sale of crude oil, natural gas, petroleum products, petrochemicals, and other specialty products; and manufactures and sells petrochemicals, including olefins, polyolefins, aromatics, and various and other petrochemicals. The company was founded in 1870 and is based in lrving, Texas.'

function NirvaHomeTarget(props) {
    const classes = useStyles()

    return (
    <Grid
    container
    direction="row"
    justify="center"
    alignItems="center"
    spacing={2}
    className={classes.root}
    >
        <Grid item xs={12}>
            <Typography variant="subtitle2" color='textSecondary'>
                Showing results for "As against its target of net zero emissions by 2025, how much has company A achieved in 2020 ?"
            </Typography>
        </Grid>
        <Grid item xs={12}>
            <CustomDataTable
                title='Emissions of Company A'
                headerData={tableHeaders}
                tableData={emissionTableData}
                actions={<></>}
                sortable={false}
            />
        </Grid>

        <Grid item xs={12}>
            <Card
                className={classes.card}
                elevation={4}
                style={ {backgroundColor: '#D3ECF6'}}
            >
                <CardContent>
                    <Typography component="p" variant="p" className={classes.title}>
                        Company A sustainability report 2015
                    </Typography>
                    <Typography
                        component="p">
                            Emission target we aim to achieve zero emission by 2025
                    </Typography>
                </CardContent>
                
            </Card>
        </Grid>










        <Grid item xs={12}>
            <Card className={classes.root} elevation={4}>


                <Grid container spacing={3} >
                    <Grid item style={{display: 'flex'}}>
                    <Grid item sx={6}>
                        <CardContent >
                            <Typography component="p" variant="p" className={classes.title}>
                                Company A Description
                            </Typography>
                            <Typography
                                component="p">
                                    {ghgCompanyDetails}
                            </Typography>
                        </CardContent>
                        <CardActions>
                            <Button style={{ textTransform: 'none' }} size="small" href="#text-buttons" color="primary">
                                {'more>>'}
                            </Button>
                        </CardActions>
                    </Grid>
                    <Grid item sx={6}>
                          <img className={classes.image} alt="complex" src={c1} />

                    </Grid>
                    </Grid>
                </Grid>
                        
                       
            </Card>
        </Grid>















        <Grid item xs={12}>
            <Typography variant="p" className={classes.title} >
                Do You Know?
            </Typography>
            <Carousel
                plugins={[
                    // 'infinite',
                    // 'arrows',
                    {
                        resolve: slidesToShowPlugin,
                        options: {
                            numberOfSlides: 3
                        }
                    },
                ]}
            >
                {corouselData && corouselData.map((item, index) => (
                    <CustomImageCard
                        key={index}
                        imgURL={item.imgURL}
                        imgAlt={item.title}
                    // title={item.title}
                    />
                ))}
            </Carousel>
        </Grid>
    </Grid>
    );
  }
  
  export default NirvaHomeTarget;