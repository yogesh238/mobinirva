import React from 'react'
import * as d3 from 'd3'

import { Paper } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    graphPaper:{
        width:400,
        margin:20,
    },
    grid: {
        color: 'lightgrey'
    }
}))

function CustomBarGraph(props) {
    const classes = useStyles()

    React.useEffect(() => {
        d3.selectAll(`.d3-bar-component-${props.barGraphName}`).select("svg").remove();
        const margin = { top: 30, right: 0, bottom: 40, left: 40 }

        let height = 200
        let width = 200
        let color = "blue"

        var yAxis = g => g
            .attr("transform", `translate(${margin.left})`)
            .call(d3.axisLeft(y).ticks(null, props.data.format).tickSizeInner(0).tickPadding(4))
            .call(g => g.select(".domain").remove())
            .call(g => g.append("text")
                .attr("x", -height / 3)
                .attr("y", -30)
                .attr("transform", "rotate(-90)")
                .attr("fill", "lightgrey")
                .attr("text-anchor", "center")
                .text(props.yAxisLabel)
            ).call(g => g.selectAll(".tick line").clone()
                .attr("x1", width)
                .attr("stroke-opacity", 0.08))

        var xAxis = g => g
            // .attr("class", classes.grid)
            .attr("transform", `translate(0,${height - margin.bottom})`)
            .call(d3.axisBottom(x)
                .tickFormat(i => props.data[i].name)
                .tickSizeInner(0)
                .tickSizeOuter(0)
                .tickPadding(8)
            ).call(g => g.select(".domain")
                .remove()
            ).call(g => g.append("text")
                .attr("x", margin.left * 3)
                .attr("y", 30)
                .attr("fill", "lightgrey")
                .attr("text-anchor", "center")
                .text(props.xAxisLabel)
            )

        var y = d3.scaleLinear()
            .domain([0, d3.max(props.data, d => d.value)]).nice()
            .range([height - margin.bottom, margin.top])

        var x = d3.scaleBand()
            .domain(d3.range(props.data.length))
            .range([margin.left, width - margin.right])
            .padding(0.5)


        const svg = d3.select(`.d3-bar-component-${props.barGraphName}`).append("svg")
            .attr("viewBox", [0, 0, width, height]);


        svg.append("g").call(xAxis);
        svg.append("g").call(yAxis);
        svg.append("g")
            .attr("fill", color)
            .selectAll("rect")
            .data(props.data)
            .join("rect")
            .attr("x", (d, i) => x(i))
            .attr("y", d => y(d.value))
            .attr("height", d => y(0) - y(d.value))
            .attr("width", 15);

        // svg.node()
    }, [])

    return (
        <>
            <Paper elevation={0} className={classes.graphPaper}>
                <div className={`d3-bar-component-${props.barGraphName}`}></div>
            </Paper>
        </>
    )
}

export default CustomBarGraph