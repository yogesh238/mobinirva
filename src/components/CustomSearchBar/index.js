import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import { InputBase, Paper, IconButton, } from '@material-ui/core';

import {
    Search as SearchIcon
} from '@material-ui/icons';

const useStyles = makeStyles({
    root: {
        padding: '2px 4px',
        display: 'flex',
        // alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'left',
        width: props => props.width || '100%',
        // height: props => props.height || 60,
        borderRadius: props => props.borderRadius || 52,
    },
    input: {
        marginLeft: props => props.iconRight ? 10 : 0,
        flex: 1,
        paddingLeft: props => props.iconRight ? 10 : 2,
        fontSize: props => props.fontSize || 12,
        color: props => props.fontSize || '#292828',
    },
    iconButton: {
        padding: 5,
        // marginRight: 10,
    },
    list: {
        cursor: 'pointer'
    }
});

const CustomSearchBar = ({ icon = null, iconRight = true, iconSize, placeholder = 'Search here...', ...props }) => {
    const classes = useStyles({ ...props, iconRight });

    return (
        <>
            <Paper component="form" className={classes.root}>
                <div>
                    {!iconRight
                        &&
                        <IconButton type="button" className={classes.iconButton}>
                            {
                                icon || <SearchIcon color='primary' fontSize={iconSize || 'small'} />
                            }
                        </IconButton>
                    }
                    <InputBase
                        className={classes.input}
                        placeholder={placeholder}
                        value={props.value}
                        onChange={(event) => props.onChange(event)}
                    />
                    {iconRight
                        &&
                        <IconButton type="button" className={classes.iconButton}>
                            {
                                icon || <SearchIcon color='primary' fontSize={iconSize || 'small'} />
                            }
                        </IconButton>
                    }
                </div>
                {
                    props.suggestions && props.suggestions.length > 0 && <div>
                        {
                            props.suggestions.map((suggestion, index) => (
                                <p id={index} key={index} className={classes.list} onClick={(event) => props.onSuggestion(event, suggestion)}>{suggestion[props.suggestionLabelKey]}</p>
                            ))
                        }
                    </div>
                }
            </Paper>
            {/* {props.suggestions && props.suggestions.length > 0 && <Paper component="ul">
                {
                    props.suggestions.map(suggestion => (
                        <li id={suggestion.companyid}>{suggestion.companyname}</li>
                    ))
                }
            </Paper>
            } */}
        </>
    );
};

CustomSearchBar.propTypes = {
    fontSize: PropTypes.number,
    borderRadius: PropTypes.number,
    iconRight: PropTypes.bool,
    icon: PropTypes.element,
    color: PropTypes.string,
    width: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
    height: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
    // selectedValue: PropTypes.string.isRequired,
};

export default CustomSearchBar;