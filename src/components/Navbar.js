import React from 'react';
import { withRouter } from 'react-router';
import { fade, makeStyles } from '@material-ui/core/styles';
import { AppBar, Toolbar, Typography } from '@material-ui/core';

import RadioButtonCheckedIcon from '@material-ui/icons/RadioButtonChecked';
import ProfileDropDown from './ProfileDropDown';
import NotificationDropDown from './NotificationDropDown';

const useStyles = makeStyles((theme) => ({
    grow: {
        flexGrow: 1
    },
    menuButton: {
        // marginRight: theme.spacing(1),
    },
    icon: {
        cursor: 'pointer'
    },
    title: {
        display: 'none',
        [theme.breakpoints.up('sm')]: {
            display: 'block',
        },
        cursor: 'pointer'
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: fade(theme.palette.common.white, 0.15),
        '&:hover': {
            backgroundColor: fade(theme.palette.common.white, 0.25),
        },
        marginRight: theme.spacing(2),
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(3),
            width: 'auto',
        },
    },
    searchIcon: {
        padding: theme.spacing(0, 2),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    inputRoot: {
        color: 'inherit',
    },
    inputInput: {
        padding: theme.spacing(1, 1, 1, 0),
        // vertical padding + font size from searchIcon
        paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: '20ch',
        },
    },
    sectionDesktop: {
        display: 'flex',
        alignItems: "center",
        justifyContent: "center",
        [theme.breakpoints.up('md')]: {
            display: 'flex',
        },
    },
    sectionMobile: {
        display: 'flex',
        [theme.breakpoints.up('md')]: {
            display: 'none',
        },
    },
    navButton: {
        marginLeft: theme.spacing(3),
        cursor: 'pointer'
    },
}));

function Navbar(props) {
    const classes = useStyles();

    return (
        <div className={classes.grow}>
            <AppBar position="static" color='transparent' elevation={0}>
                <Toolbar>
                    <RadioButtonCheckedIcon color='primary' className={classes.icon} />
                    <Typography className={classes.title} variant="h6" noWrap onClick={() => props.history.push('/nirvahome')}>
                        NIRVA
                    </Typography>

                    <div className={classes.grow} />
                    <div className={classes.sectionDesktop}>
                        <div className={classes.navButton}>ESG Report</div>
                        <div className={classes.navButton}>ESG Screens</div>
                        <div className={classes.navButton} onClick={() => props.history.push('/fleximandate')}>Flexi Mandate</div>
                        <div className={classes.navButton}>Portfolio Analytics</div>
                        <div className={classes.navButton}>Client Dashboard</div>
                        <div className={classes.navButton} onClick={() => props.history.push('/controversy')}>Controversy Module</div>

                        <NotificationDropDown />
                        <ProfileDropDown props={props} />
                    </div>
                </Toolbar>
            </AppBar>
        </div>
    );
}

export default withRouter(Navbar);