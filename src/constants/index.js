
// Colors
export const COLORS = {
    PRIMARY: '#0A67D4', // blue
    PRIMARY2: '#343536', // credblack
    SECONDARY: '#FFFFFF', // white
    SECONDARY2: '#F7F7FB',
    SECONDARY3: '#F6F6F6',
    ORANGE: '#FE7200', // orange
    DARKORANGE: '#FA5028', // dark orange
    CREDBLACK: '#202427', // credblack
    WHITE: '#FFFFFF', // white
    BLACK: '#000000', // black
    BACK_BUTTON_UNDERLAY_COLOR: '#DDDDDD',
};