import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import {
    Button, IconButton, Grid, Box, Paper, TextField, Typography,
    Accordion, AccordionDetails, AccordionSummary
} from '@material-ui/core';
import {
    Check as CheckIcon, Close as CloseIcon,
    ExpandMore as ExpandMoreIcon,
    Assistant as AssistantIcon,
} from '@material-ui/icons';

import DropDownMenu from '../../components/DropDownMenu';
import CustomModal from '../../components/Modal/CustomModal';
import CustomSearchBar from '../../components/CustomSearchBar';
import CustomChipComponent from '../../components/CustomChipComponent';

import AddCompany from '../AddCompany';

import { COLORS } from '../../constants';
import { DATA, MANDATEDATA } from './PortFolioData';


const useStyles = makeStyles((theme) => ({
    addPortFolioCard: {
        display: 'flex',
        // backgroundColor: 'red',
        alignItems: 'flex-end',
        padding: theme.spacing(2),
        width: '80%'
    },
    paper: {
        // flexGrow: 1,
        backgroundColor: COLORS.SECONDARY2,
        // width: '80%',
        borderRadius: theme.spacing(1),
        padding: 20,
        margin: 0,
    },
    actionBtns: {
        // backgroundColor:'red'
    },
    iconCheck: {
        backgroundColor: theme.palette.primary.main || '#0A67D4',
        color: COLORS.SECONDARY,
        borderRadius: theme.spacing(1),
        marginLeft: 0,
        padding: theme.spacing(1),
        fontSize: 40,

    },
    iconClose: {
        backgroundColor: COLORS.SECONDARY3,
        color: '#4F4E4E',
        borderRadius: theme.spacing(1),
        marginLeft: 0,
        padding: theme.spacing(1),
        fontSize: 40,

    },
    iconBtn: {
        // backgroundColor: theme.palette.primary.main,
        marginLeft: theme.spacing(2),
        padding: 0,
        borderRadius: 0,

    },
    button: {
        textTransform: 'none',
        fontSize: 14,
        // marginRight: theme.spacing(3)
    },
    mandateGrid: {
        display: 'flex',
        flexDirection: 'row',
        justifyItems: 'center',
        alignItems: 'center',
        '& p': {
            color: COLORS.PRIMARY2,
            marginRight: theme.spacing(1),
        },
    },
    accordionItemGrid: {
        display: 'flex',
        alignItems: 'flex-end',
        // backgroundColor: 'yellow',
        padding: 0,
        marginBottom: theme.spacing(2),
    },
    accordionItem: {
        flexGrow: 1,
        backgroundColor: COLORS.SECONDARY2,
        // width: '80%',
        // backgroundColor: 'blue',
        borderRadius: theme.spacing(1),
        marginBottom: 0,
        // padding: 20,
    },
    heading: {
        flexGrow: 2,
        fontSize: 16,
        fontWeight: 'bold'
    },
    secondaryHeading: {
        fontSize: 14,
        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(2),
    }
}))

const AddOrEditPortfolio = (props) => {
    const classes = useStyles()

    const [modalOpen, setModalOpen] = React.useState(false);
    const [ShowAddPortfolio, setShowAddPortfolio] = React.useState(false);
    const [searchValue, setSearchValue] = React.useState('');
    const [expanded, setExpanded] = React.useState(false);
    const [portfolios, setPortfolios] = React.useState(DATA);
    const [portfolioForm, setPortfolioForm] = React.useState({
        portfolioid: '',
        portfolioname: '',
        mandateid: '',
        mandatename: '',
        companies: []
    });

    const onSearch = (event) => {
        setSearchValue(event.target.value)
    }

    const handleChange = (panel, data) => (event, isExpanded) => {
        setExpanded(isExpanded ? panel : false);
        setPortfolioForm(data)
    };

    // portfolio call back functtions
    const arrangePortfolioData = (data) => {
        setPortfolioForm(data)
    }

    const onPortfolioChange = (event) => {
        setPortfolioForm({
            ...portfolioForm,
            portfolioid: portfolios.length ? portfolios.length + 1 : 1,
            portfolioname: event.target.value
        })
    }

    const onPortfolioMandateChange = (mandate) => {
        setPortfolioForm({
            ...portfolioForm,
            mandateid: mandate.value,
            mandatename: mandate.label,
        })
    }

    const onAddCompanyHandler = (event, companies) => {
        setPortfolioForm({
            ...portfolioForm,
            companies: companies
        })
    }

    // this function will be called whenever you check or uncheck a company
    const onCompanyClick = (event, data, value) => {
        let c = portfolioForm.companies.filter(company => company.companyid !== value.companyid)
        setPortfolioForm({
            ...portfolioForm,
            companies: c
        })
    }

    const handleSave = (type, data) => {
        setExpanded(false)
        setShowAddPortfolio(false)
        if (type === 'new') {
            setPortfolios([portfolioForm, ...portfolios])
        } else {
            let p = portfolios.map(port => {
                if (port.portfolioid === data.portfolioid) return portfolioForm;
                else return port;
            })
            setPortfolios(p)
        }
    }

    const handleClose = () => {
        setExpanded(false)
        setShowAddPortfolio(false)
    }

    const handleModalClose = () => {
        setModalOpen(false);
    }

    return (
        <>
            <Button
                variant="outlined"
                color="primary"
                size="small"
                fullWidth
                className={classes.button}
                onClick={() => { setModalOpen(true) }}
            >
                {'Add / Edit'}
            </Button>
            <CustomModal
                title="Portfolio"
                open={modalOpen}
                maxWidth='md'
                onClose={handleModalClose}
            // onAdd={onAddHandler}
            >
                <Grid container spacing={3}>
                    <Grid item xs={12}>
                        <CustomSearchBar
                            borderRadius={52}
                            width={'34%'}
                            height={40}
                            fontSize={18}
                            iconSize='medium'
                            placeholder="Search"
                            value={searchValue}
                            onChange={onSearch}
                            // icon={null}
                            iconRight={false}
                        />
                    </Grid>
                    <Grid item xs={12} >
                        <Button
                            variant="text"
                            color="primary"
                            size="medium"
                            className={classes.button}
                            onClick={() => {
                                setShowAddPortfolio(show => !show);
                                setPortfolioForm({
                                    portfolioid: '',
                                    portfolioname: '',
                                    mandateid: '',
                                    mandatename: '',
                                    companies: []
                                })
                            }}
                        >
                            {'+Add Portfolio'}
                        </Button>
                        {ShowAddPortfolio && <AddPortFolioCard
                            portfolioForm={portfolioForm}
                            onPortfolioChange={onPortfolioChange}
                            onPortfolioMandateChange={onPortfolioMandateChange}
                            onAddCompanyHandler={onAddCompanyHandler}
                            onCompanyClick={onCompanyClick}
                            handleSave={handleSave}
                            handleClose={handleClose}
                        />}
                    </Grid>
                    <Grid item xs={12}>
                        <Grid container spacing={3} className={classes.addPortFolioCard} >
                            {portfolios.map((item) => <PortfolioAccordion
                                id={item.portfolioid}
                                data={item}
                                expanded={expanded}
                                portfolioForm={portfolioForm}
                                arrangePortfolioData={arrangePortfolioData}
                                companies={item.companies}
                                onCompanyClick={onCompanyClick}
                                onAccordionChange={handleChange}
                                onPortfolioChange={onPortfolioChange}
                                onPortfolioMandateChange={onPortfolioMandateChange}
                                onAddCompanyHandler={onAddCompanyHandler}
                                handleSave={handleSave}
                                handleClose={handleClose}
                            />)}
                        </Grid>
                    </Grid>
                </Grid>
            </CustomModal>
        </>
    )
}

const AddPortFolioForm = ({ data, onPortfolioChange, onPortfolioMandateChange, onAddCompanyHandler, onCompanyClick }) => {
    const classes = useStyles();

    const onCompanyDelete = (event, company) => {
        onCompanyClick(event, data, company)
    }

    return (
        <Grid container spacing={3}>
            <Grid item xs={12}>
                <TextField
                    id="portfolio-name-input"
                    label="Portfolio name"
                    placeholder="Enter portfolio name"
                    fullWidth
                    margin="normal"
                    InputLabelProps={{
                        shrink: true,
                    }}
                    value={data.portfolioname}
                    onChange={onPortfolioChange}
                />
            </Grid>
            <Grid item xs={12} className={classes.mandateGrid}>
                <Typography variant="body1" component="p" >Mandate</Typography>
                <DropDownMenu
                    title="Select Mandate"
                    value={data.mandatename}
                    options={[
                        ...MANDATEDATA.map(item => ({ id: item.mandateid, label: item.mandatename, value: item.mandateid })),
                    ]}
                    onChange={onPortfolioMandateChange}
                    btnStyles={{
                        backgroundColor: COLORS.SECONDARY, color: COLORS.PRIMARY2
                    }}
                    size="medium"
                />
            </Grid>

            <Grid item xs={12} >
                <AddCompany
                    defaultCompanies={data.companies}
                    addHandler={onAddCompanyHandler}
                />
            </Grid>
            <Grid item xs={12} >
                {data.companies &&
                    <CustomChipComponent
                        chipData={data.companies}
                        labelkey='companyname'
                        chipColor='default'
                        variant='outlined'
                        backgroundColor='#F7F7FB'
                        chipAlign='left'
                        chipLabelLetterSize={20}
                        handleDelete={onCompanyDelete}
                    />
                }
            </Grid>
        </Grid>
    )
}

const AddPortFolioCard = ({ hideSubmitButtion, portfolioForm, onPortfolioChange, onPortfolioMandateChange, onAddCompanyHandler, onCompanyClick, handleSave, handleClose }) => {
    const classes = useStyles();

    return (
        <Grid container spacing={3} className={classes.addPortFolioCard}>
            <Grid xs={10} >
                <Paper elevation={0} square className={classes.paper}>
                    <AddPortFolioForm data={portfolioForm} onPortfolioChange={onPortfolioChange} onPortfolioMandateChange={onPortfolioMandateChange} onAddCompanyHandler={onAddCompanyHandler} onCompanyClick={onCompanyClick} />
                </Paper>
            </Grid>
            <Grid xs={2}  >
                {/* <ActionButtons /> */}
                <Box className={classes.actionBtns}>
                    {!hideSubmitButtion &&
                        <IconButton color="primary" aria-label="ok" className={classes.iconBtn} component="span" onClick={() => handleSave('new', portfolioForm)}>
                            <CheckIcon className={classes.iconCheck} />
                        </IconButton>
                    }
                    <IconButton color="default" aria-label="clear" className={classes.iconBtn} component="span" onClick={handleClose}>
                        <CloseIcon className={classes.iconClose} />
                    </IconButton>
                </Box>
            </Grid>
        </Grid>
    )
}

const PortfolioAccordion = ({ data, id, expanded, onAccordionChange, portfolioForm, onPortfolioChange, onPortfolioMandateChange, onAddCompanyHandler, onCompanyClick, handleSave, handleClose }) => {
    const classes = useStyles();
    let isOpen = expanded === id;

    return (
        <Grid key={id} item xs={12} className={classes.accordionItemGrid} style={{ padding: 0, }} >
            <Grid item xs={10}>
                <Accordion elevation={0} className={classes.accordionItem} expanded={isOpen} onChange={onAccordionChange(id, data)}>
                    <AccordionSummary
                        expandIcon={<ExpandMoreIcon />}
                        aria-controls="panel-content"
                        id="panel1bh-header"
                    >
                        <Typography className={classes.heading}>{data.portfolioname}</Typography>
                        {!isOpen && <AssistantIcon color="primary" />}
                        {!isOpen && <Typography className={classes.secondaryHeading}>{data.mandatename}</Typography>}
                    </AccordionSummary>
                    <AccordionDetails>
                        <AddPortFolioForm data={portfolioForm} onPortfolioChange={onPortfolioChange} onPortfolioMandateChange={onPortfolioMandateChange} onAddCompanyHandler={onAddCompanyHandler} onCompanyClick={onCompanyClick} />
                    </AccordionDetails>
                </Accordion>
            </Grid>
            <Grid item xs={2}>
                <Box className={classes.actionBtns}>
                    {isOpen &&
                        <IconButton color="primary" aria-label="ok" className={classes.iconBtn} component="span" onClick={() => handleSave('exsisting', data)}>
                            <CheckIcon className={classes.iconCheck} />
                        </IconButton>
                    }
                    <IconButton color="default" aria-label="clear" className={classes.iconBtn} component="span" onClick={handleClose}>
                        <CloseIcon className={classes.iconClose} />
                    </IconButton>
                </Box>
            </Grid>
        </Grid >
    )
}

export default AddOrEditPortfolio;