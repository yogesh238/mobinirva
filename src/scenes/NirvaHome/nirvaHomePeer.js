import { Typography, Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import {
    Card,
    CardContent
} from '@material-ui/core';

import CustomBarGraph from '../../components/CustomBarGraph';
import CustomDataTable from '../../components/CustomDataTable';


const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
      },
      paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
      },

}));



export const peerGraphData = [
    {
        name: 'A',
        value: 9
    },
    {
        name: 'B',
        value: 5
    },
    {
        name: 'C',
        value: 7
    },
    {
        name: 'D',
        value: 3
    },
    {
        name: 'E',
        value: 1
    }
]

export const peerTableData =[
    {
        label:'Company A',
        '1':'15',
        '2':'7',
        '3':'46.66',
    },
    {
        label:'Company B',
        '1':'9',
        '2':'3',
        '3':'33.33',
    },
    {
        label:'Company C',
        '1':'12',
        '2':'5',
        '3':'41.66',
    },
    {
        label:'Company D',
        '1':'12',
        '2':'5',
        '3':'41.66',
    },
    {
        label:'Company E',
        '1':'9',
        '2':'3',
        '3':'33.33',
    },
]


export const peerTableHeaders = [
    {
        label: '',
        key: 'label',
        sort: false
    },
    {
        label: 'No. of board Members',
        key: '1',
        sort: false
    },
    {
        label: 'No of women',
        key: '2',
        sort: false
    },
    {
        label: 'Women Participation rate(%)',
        key: '3',
        sort: false
    }
]

function NirvaHomePeer(props) {
    const classes = useStyles()

    return (
    <Grid container spacing={3}>
            <Grid item xs={12} lg={12} md={12} >
                <Typography variant="subtitle2" color='textSecondary'>
                    Showing GHG Emission of Company AB from 2015-2021
                </Typography>
            </Grid>

            <Grid item xs={12} sm={6} >    
            <CustomDataTable
                    title='Peer Comparison'
                    headerData={peerTableHeaders}
                    tableData={peerTableData}
                    actions={<></>}
                    sortable={false}
                />
               
            </Grid>
            <Grid item xs={12} sm={6} >
                <Card
                    elevation={4} 
                >
                <CardContent>
                    <Typography component="p" variant="p" className={classes.graphTitle}>
                        Women Participation
                    </Typography>
                    <CustomBarGraph
                        barGraphName='emission'
                        data={peerGraphData}
                        xAxisLabel='Company'
                        yAxisLabel='No of Womens'
                    />
                </CardContent>
                </Card>
               
            </Grid>
    
        
    </Grid>
      
    );
  }
  
  export default NirvaHomePeer;