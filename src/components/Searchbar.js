import React from 'react';

import SearchIcon from '@material-ui/icons/Search';
import { makeStyles } from '@material-ui/core/styles';
import { Paper, InputBase, IconButton } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    root: {
        padding: '2px 4px',
        display: 'flex',
        alignItems: 'center',
        width: '100%',
        height:'60px',
        borderRadius:'52px',
        // backgroundColor:'red'
    },
    input: {
        marginLeft: theme.spacing(1),
        flex: 1,
        paddingLeft:20,
        fontSize:18,
        color:'#292828',
    },
    iconButton: {
        padding: 10,
        marginRight:10,
    },
}));

export default function Searchbar(props) {
    const classes = useStyles();

    return (
        <Paper component="form" className={classes.root}>
            <InputBase
                className={classes.input}
                placeholder="nirva insta"
                value={props.value}
            />
            <IconButton type="button" className={classes.iconButton}>
                <SearchIcon  color='primary' fontSize='large'  />
            </IconButton>
        </Paper>
    );
}