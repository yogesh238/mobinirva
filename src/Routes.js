import React from "react";

import {
	BrowserRouter as Router,
	Switch,
	Route,
	Redirect,
} from "react-router-dom";

import Home from './scenes/Home'
import './App.css';

import ProtectedRoutes from './ProtectedRoutes'

const Routes = (props) => {
  return (
    <Router>
        <Switch>
            <Route exact path='/'>
                <Redirect to='/home' />
            </Route>
            <Route exact path='/home' component={Home} />
            <ProtectedRoutes />
            
        </Switch>
    </Router>
  );
};

export default Routes;
