import React, { useState, useEffect } from 'react';
import Radar from 'react-d3-radar';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import {
    Button, Grid,
    Typography, TextField,
    Card, CardContent,
    Input,
    FormControlLabel, Radio, RadioGroup,
    Checkbox,
} from '@material-ui/core';
import {
    Assistant as AssistantIcon,
} from '@material-ui/icons';

import CustomTreeView from '../../components/TreeView';
import CustomModal from '../../components/Modal/CustomModal';
import CustomRadarChart from '../../components/CustomRadarChart';

import { DATA, MANDATEDATA, taxonomyData, ESG_DATA } from './mandateData';

import { COLORS } from '../../constants';
import { genId } from '../../utils/helperFuncs';

const useStyles = makeStyles((theme) => ({

    button: {
        textTransform: 'none',
        fontSize: 14,
        // marginRight: theme.spacing(3)
    },
    secondaryHeading: {
        fontSize: 14,
        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(2),
    },
    mandateCard: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: COLORS.SECONDARY2,
        borderRadius: 2,
    },
    mandateCardHead: {
        fontSize: 18,
        fontWeight: 'bold',
        color: COLORS.PRIMARY,
        marginBottom: theme.spacing(1),
        marginTop: theme.spacing(1),
    },
    mandateLi: {
        display: 'flex',
        cursor: 'pointer',
        alignItems: 'center',
        marginBottom: theme.spacing(3),
    },
    addMandateGrid: {
        padding: theme.spacing(3),
        // paddingLeft: theme.spacing(3),
        // paddingRight: theme.spacing(3),
    },
    addMandateTitle: {
        fontSize: 18,
        fontWeight: 'bold',
        color: '#131111',
        marginBottom: theme.spacing(2),
    },
    graphContainer: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        // padding: 50,
        // width: '80%',
        height: 350,
    },
    addMandateForm: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    inputContainer: {
        marginTop: 5,
        marginBottom: 5,
    },
    inputDiv: {
        marginLeft: 5,
        marginRight: 10,
    },
    inputTextField: {
        maxWidth: 40,
        padding: 2,
        paddingLeft: 4,
        border: '1px solid #707070',
        textAlign: 'center'

    }
}))

const makeData = (items, esgDims, onChangeEsgDims) => {
    console.log("mandate esg items", items);
    if (items.length === 0 && !Array.isArray(items)) {
        return [];
    }

    return items.map((item, i) => {

        let grp = {
            id: `grp-${i}-${item.id}`,
            children: [],
        }
        let grpValue = 0;

        grp.children = item.categories.map((category, j) => {
            let value = esgDims[category.key];
            grpValue = grpValue + (parseFloat(value) || 0);
            return {
                id: `catg-${j}-${category.id}`,
                name: <BuildComponent id={`catg-${j}-${category.id}`} name={category.key} value={value} onChange={onChangeEsgDims} itemId={category.id} label={category.name} item={category} />,
                children: [],
            }
        });
        grp.name = <BuildComponent inputDisabled={true} id={`grp-${i}-${item.id}`} name={item.key} value={grpValue} itemId={item.id} label={item.name} item={item} />;

        return grp;
    });
}

const AddOrEditMandate = (props) => {
    const classes = useStyles()

    const defaultGraphData = {
        labels: ['Environment', 'Social Capital', 'Human Capital', 'Business Model', 'Leadership'],
        data: [0, 0, 0, 0, 0]
    }

    const [modalOpen, setModalOpen] = useState(false);
    const [mandateData, setMandateData] = useState([]);
    const [mandate, setMandate] = useState(null)
    const [taxonomy, setTaxonomy] = useState([]); // taxonomy data
    const [taxonomyId, setTaxonomyId] = useState(''); // selected taxonomyId

    const [esgDims, setEsgDims] = useState([]);
    const [esgDimsMkData, setEsgDimsMkData] = useState([]);
    const [formData, setFormData] = useState({
        mandateid: '',
        mandatename: '',
        taxonomyid: '',
        taxonomyname: '',
        esg_dimentions: {
        }
    });
    const [isEdit, setIsEdit] = useState(false);
    // const [dataForTree, setDataForTree] = useState(makeData(MANDATEDATA[0].esg_dimension))
    const [defaultExpandNodes, setDefaultExpandNodes] = useState([]);
    const [domainMax, setdomainMax] = useState(100);
    const [graphData, setGraphData] = React.useState(defaultGraphData);

    const onChangeEsgDims = (e) => {
        // console.log("", { target, val: target.value, name: target.name })
        e.stopPropagation();
        let name = e.target.name;
        let value = e.target.value;
        setFormData({ ...formData, esg_dimentions: { ...formData.esg_dimentions, [name]: value } })
    }

    useEffect(() => {
        setMandateData(MANDATEDATA);
        setTaxonomy(taxonomyData);
        resetAddMandateForm();
        // setTaxonomyId(taxonomyData[0].taxonomyid || 'nirva');
        // setFormData({ ...formData,  })
    }, []);

    useEffect(() => {
        console.log("useEffect(() => setEsgDimsMkData ", esgDims, formData)
        setEsgDimsMkData(makeData(esgDims, formData.esg_dimentions, onChangeEsgDims));
    }, [formData.esg_dimentions, esgDims])

    useEffect(() => {
        if (taxonomyId) {
            let esgDim = [];
            let formDt = {
                mandateid: genId(),
                mandatename: '',
                taxonomyid: taxonomyId,
                taxonomyname: '',
                esg_dimentions: {},
            };
            if (isEdit) {
                // esgDim = mandate && mandate.esg_dimension && JSON.parse(JSON.stringify(mandate.esg_dimension));
                esgDim = mandate && JSON.parse(JSON.stringify(mandate.esg_dimension || []));
                formDt.mandateid = mandate.mandateid || '';
                formDt.mandatename = mandate.mandatename || '';
                formDt.taxonomyname = mandate.assosiatedtaxonomyname || '';
            } else {
                let selTaxonomy = taxonomy.find(tx => tx.taxonomyid === taxonomyId);
                formDt.taxonomyname = selTaxonomy.taxonomyname || '';
                esgDim = selTaxonomy && selTaxonomy.esg_dimension && JSON.parse(JSON.stringify(selTaxonomy.esg_dimension));
            }
            console.log("useEffect(() => ", { esgDim, taxonomyId, taxonomy, isEdit, mandate, formDt })

            esgDim && esgDim.forEach(grp => {
                grp.categories && grp.categories.forEach(catg => {
                    formDt.esg_dimentions[catg.key] = catg.value || 0;
                });
            }); // creating esg_dimenstion form data with key and value pairs ex: { afford:10, welfare:15,}
            setFormData(formDt)
            esgDim && setEsgDims(esgDim);
            // esgDim && setEsgDimsMkData(makeData(esgDim, formDt, onChangeEsgDims));
            console.log("useEffect(() => after ", { esgDim, taxonomyId, taxonomy, isEdit, mandate, formDt })
        }

    }, [taxonomyId, isEdit, taxonomy, mandate])

    const handleOnMandateNameChange = ({ target }) => {
        let val = target.value; // handle text validation
        // let id = mandate.mandateid || genId();
        setMandate({
            // mandateid: id,
            ...mandate,
            mandatename: val,
        })
    }

    const handleTaxonomyChange = ({ target }) => {
        // console.log("handleTaxonomyChange", { name: target.name, val: target.value });
        setTaxonomyId(target.value);
    }

    const resetAddMandateForm = () => {
        let id = genId();
        setIsEdit(false);
        setMandate({
            mandateid: id,
            mandatename: '',
        })
        setEsgDims([])
        setEsgDimsMkData([])
        setFormData({
            mandateid: id,
            mandatename: '',
            taxonomyid: '',
            taxonomyname: '',
            esg_dimentions: {}
        })
        setTaxonomyId(taxonomyData[0].taxonomyid || 'nirva');
        console.log("  resetAddMandate = () => ", { mandate, formData, taxonomyId });
        setGraphData(defaultGraphData)
    }

    const handleAddMandate = (e) => {
        console.log("  handleAddMandate = () => ", { e });
        resetAddMandateForm();
    }

    const handleOnMandateClick = (item) => (e) => {
        console.log(" handleOnMandateClick = (item) => () =>", { item, e, })
        let sMandate = mandateData.find(m => m.mandateid === item.mandateid) // sMandate = selected mandate
        if (sMandate) {
            /* for graph start [yogesh] */
            let d = [], l = [], maxVal = 0;
            if (sMandate.esg_dimension && sMandate.esg_dimension.length) {
                l = sMandate.esg_dimension.map(esgDim => {
                    let val = esgDim.value ? Math.floor(parseInt(esgDim.value)) : 0;
                    maxVal = maxVal < val ? val : maxVal;
                    d.push(val)
                    return esgDim.shortname
                })
            }
            if (d.length && l.length){
                setGraphData({ data: d, labels: l });
            } else setGraphData(defaultGraphData)
            setdomainMax(maxVal + 20);
            /* for graph end [yogesh] */
            setIsEdit(true);
            setMandate({
                ...sMandate,
            });
            setTaxonomyId(sMandate.assosiatedtaxonomyid || 'nirva');
        }
    }

    // const onMandateClick = (mandate) => {
    //     console.log("it is mandate click", mandate);
    //     let v = [], s = [];
    //     if (mandate.esg_dimension && mandate.esg_dimension.length) {
    //         let obj = {
    //             key: 'esg_dimension',
    //             label: 'ESG Dimension',
    //             values: {}
    //         }
    //         v = mandate.esg_dimension.map(item => {
    //             obj.values[item.key] = item.value ? item.value : 0
    //             return {
    //                 key: item.key,
    //                 label: item.name
    //             }
    //         })
    //         s.push(obj)
    //         setGraphData({ variables: v, sets: s })
    //     }
    // }

    const handleModalClose = () => {
        setModalOpen(false);
    }

    const handleOnAddOrSave = () => {
        console.log("handleOnAddOrSave =()=> ", { mandate, taxonomy, taxonomyId, esgDims, formData });
        let newMandate = { ...mandate };
        let mandateId = mandate.mandateid || '';
        if (newMandate.mandatename.trim() === '') {
            alert("Please enter mandate name");
            return false;
        }
        if (esgDims.length === 0) {
            alert("Please select taxonomy and ESG Dimensions");
            return false;
        }

        let newEsgDims = esgDims.map(esg => {
            let esgValue = 0;
            esg.categories = esg.categories.map(catg => {
                catg.value = formData.esg_dimentions[catg.key];
                esgValue = esgValue + (parseFloat(formData.esg_dimentions[catg.key]) || 0);
                return catg;
            });
            esg.value = esgValue;
            return esg;
        });
        newMandate.esg_dimension = newEsgDims;
        newMandate.assosiatedtaxonomyid = formData.taxonomyid;
        newMandate.assosiatedtaxonomyname = formData.taxonomyname;
        console.log("handleOnAddOrSave =()=> after ", { newMandate, newEsgDims })
        if (isEdit) {
            console.log("isedit -> ", isEdit);
            let mData = mandateData.slice();
            let mandateIndex = mData.findIndex(m => m.mandateid === mandateId);
            if (mandateIndex !== -1) {
                mData[mandateIndex] = newMandate;
            }
            console.log("isedit index-> ", { mData, mandateIndex, newMandate, mandateId });
            setMandateData(mData);
        } else {
            console.log("isedit -> ", isEdit)
            setMandateData([...mandateData, newMandate])
        }
        resetAddMandateForm();
    }

    const onNodeToggle = (event, nodes) => {
        setDefaultExpandNodes(nodes)
    }

    return (
        <>
            <Button
                variant="outlined"
                color="primary"
                size="small"
                fullWidth
                className={classes.button}
                onClick={() => { setModalOpen(true) }}
            >
                {'Add / Edit'}
            </Button>
            <CustomModal
                title="Mandate"
                open={modalOpen}
                maxWidth='md'
                onClose={handleModalClose}
                onAdd={handleOnAddOrSave}
            >
                <Grid container spacing={3}>
                    <Grid item xs={12} md={4}>
                        <MandateListCard list={mandateData} onAddMandate={handleAddMandate} onClickMandate={handleOnMandateClick} />
                    </Grid>
                    <Grid item xs={12} md={8} >
                        <Grid container spacing={1} className={classes.addMandateGrid} >
                            <Grid item xs={12} >
                                <Typography className={classes.addMandateTitle}>{mandate?.mandatename || 'Add Mandate'} </Typography>
                            </Grid>
                            <Grid item xs={12}>
                                {/* <div className={classes.graphContainer}> */}
                                <CustomRadarChart
                                    data={graphData.data}
                                    labels={graphData.labels}
                                />
                                {/* <Radar
                                        width={450}
                                        height={450}
                                        padding={50}
                                        domainMax={domainMax}
                                        // radius={20}
                                        highlighted={null}
                                        // style={{ numRings: 4 }}
                                        style={{ color: 'red' }}
                                        onHover={(point) => {
                                            if (point) {
                                                console.log('hovered over a data point');
                                            } else {
                                                console.log('not over anything');
                                            }
                                        }}
                                        data={graphData}
                                    /> */}
                                {/* </div> */}
                            </Grid>
                            <Grid item xs={12} >
                                <form noValidate autoComplete="off">
                                    <Grid container spacing={3} className={classes.addMandateForm}>
                                        <Grid item xs={4} >
                                            <Typography component="span" >Name of Mandate</Typography>
                                        </Grid>
                                        <Grid item xs={8} >
                                            <TextField id="mandate-name" label="Enter mandate name" variant="outlined" value={mandate?.mandatename || ''} onChange={handleOnMandateNameChange} />
                                        </Grid>
                                        <Grid item xs={4} >
                                            <Typography component="span" >Taxonomy</Typography>
                                        </Grid>
                                        <Grid item xs={8} >
                                            <RadioGroup row aria-label="Taxonomy" name="Taxonomy" defaultValue='nirva'
                                                value={taxonomyId} onChange={handleTaxonomyChange}
                                            >
                                                {taxonomy.map(tx => (
                                                    <FormControlLabel value={tx.taxonomyid} control={<Radio color="primary" />} label={tx.taxonomyname} />
                                                ))}
                                                {/* <FormControlLabel value="SASB" control={<Radio color="primary" />} label="SASB" />
                                                <FormControlLabel value="GRI" control={<Radio color="primary" />} label="GRI" /> */}
                                            </RadioGroup>
                                        </Grid>
                                    </Grid>
                                </form>
                            </Grid>
                            {/* <Grid item xs={12} >
                            </Grid> */}
                            <Grid item xs={12} >
                                <Typography className={classes.addMandateTitle}>ESG Dimension </Typography>
                                <CustomTreeView items={esgDimsMkData} defaultExpandNodes={defaultExpandNodes} onNodeToggle={onNodeToggle} />
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </CustomModal>
        </>
    )
}

const MandateListCard = ({ list, onAddMandate, onClickMandate, ...props }) => {
    const classes = useStyles();
    const defaultGraphData = {
        labels: ['Environment', 'Social Capital', 'Human Capital', 'Business Model', 'Leadership'],
        data: [0, 0, 0, 0, 0]
    }
    return (
        <Card elevation={1} className={classes.mandateCard}  >
            <CardContent>
                <Typography className={classes.mandateCardHead}>Mandates list</Typography>
                <Button
                    aria-label="add-mandate-button"
                    aria-controls="add-mandate-button"
                    // aria-haspopup="true"
                    variant={"outlined"}
                    size={"large"}
                    color={"primary"}
                    onClick={onAddMandate}
                    // endIcon={endIcon}
                    className={classes.button}
                >
                    Add Mandate
                </Button>
                <ul style={{ padding: 5 }}>
                    {list && Array.isArray(list) && list.map((item, i) => {
                        return (
                            <li key={item.mandateid || i} className={classes.mandateLi} onClick={onClickMandate(item)} >
                                {/* <AssistantIcon color="primary" /> */}
                                <CustomRadarChart
                                height = {50}
                                labels={['','','','','']}
                                data={defaultGraphData.data}
                                miniChart={true}
                                numberOfGrids={3}
                                />
                                <Typography className={classes.secondaryHeading}>{item.mandatename}</Typography>
                            </li>
                        )
                    })}
                </ul>
            </CardContent>
        </Card>
    )
}

const BuildComponent = withStyles((theme) => ({
    inputContainer: {
        marginTop: 5,
        marginBottom: 5,
    },
    inputDiv: {
        marginLeft: 5,
        marginRight: 10,
    },
    inputTextField: {
        maxWidth: 40,
        padding: 2,
        paddingLeft: 4,
        border: '1px solid #707070',
        textAlign: 'center'
    }
}))(({ id, name, value, onChange, itemId, label, item, inputDisabled, classes, ...props }) => {
    // const classes = useStyles();

    return (
        <div key={id} id={itemId} className={classes.inputContainer}>
            {/* <TextField
                // label="Dense"
                id={label + '-' + id || genId()}
                defaultValue="0"
                className={classes.inputTextField}
                // helperText="Some important text"
                margin="dense"
                size="small"
                variant="outlined"
                onChange={(event) => onClick ? onClick(event, value) : null}
            // style={{ maxWidth: 50 }}
            /> */}
            <Input
                // defaultValue="00" 
                inputProps={{
                    'aria-label': 'description',
                    className: classes.inputTextField
                }}
                className={classes.inputDiv}
                id={label + '-' + id || genId()}
                name={name}
                placeholder="00"
                variant="outlined"
                value={value}
                disabled={inputDisabled}
                onChange={typeof onChange === 'function' ? onChange : null}
            />
            <Typography variant="inherit" component="span" >{label || 'test label'}</Typography>
        </div>
    )
})

export default AddOrEditMandate;