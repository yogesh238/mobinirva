import React from 'react';

import {
    Container,
    Grid,
    // Typography,
    // Paper,
    // Button, Card, CardActions, CardContent
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import coverImage from '../../assets/home-banner.png';
import nirvaTitle from '../../assets/nirva-title.png';

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexGrow: 1,
        backgroundColor: '#FFFFFF',
        height: "100vh",
        paddingTop: 40,
        paddingLeft: 30,
        paddingRight: 30,
        // backgroundColor: 'yellow',
    },
    nirvaTitle: {
        display: 'flex', alignItems: 'center',
        justifyContent: "center", width: '100%',
    },
    nav: {
        display: "flex",
        // alignItems: 'center',
        // alignContent:'center',
        justifyContent: "center",
        padding: theme.spacing(4, 1, 3, 0),
        // backgroundColor:'red'

    },
    navPrimeButton: {
        cursor: 'pointer',
        color: '#343536',
        fontSize: 18,
    },
    navButton: {
        marginLeft: theme.spacing(4),
        cursor: 'pointer',
        color: '#343536',
        fontSize: 18,
    },
    image: {
        position: 'relative',
        padding: theme.spacing(4),
        height: "500px",
        width: "1200px"
    },
    coverImg: {
        // height:'100vh',
        width: '100%',
        backgroundColor: 'yellow'
    },
    coverContainer: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#e2e2e2',
        height: '100%'
    },
    coverTitleSection: {
        // styles
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 20,
    },
    coverTitle: {
        // styles
        fontSize: '33px'
    },
    coverSubTitle: {
        // styles
    },

}))

export default function Navbar(props) {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Container maxWidth="xl" >
                <Grid
                    container
                    direction="row"
                    justify="center"
                    alignItems="center"
                    spacing={2}
                    className={classes.row}
                >
                    <Grid container >
                        <div className={classes.nirvaTitle} >
                            <img src={nirvaTitle} alt='cover' width={250} />
                        </div>
                    </Grid>
                    <Grid container>
                        <Grid item xs={12} className={classes.nav} >
                            <div className={classes.navPrimeButton}>HOME</div>
                            <div className={classes.navButton}>TEAM/LEADERSHIP</div>
                            <div className={classes.navButton}>PRODUCTS</div>
                            <div className={classes.navButton}>CONTACT US</div>
                            <div className={classes.navButton} onClick={() => props.history.push('/nirvahome')}>SIGN IN</div>
                        </Grid>
                    </Grid>
                    <Grid container>
                        <Grid item xs={12} >
                            <div className={classes.coverContainer}>
                                <img src={coverImage} alt='cover' className={classes.coverImg} />
                            </div>
                        </Grid>
                        {/* <Grid item xs={4} >
                            <div className={classes.coverContainer}>
                                <div className={classes.coverTitleSection} >
                                    <div className={classes.coverTitle} >Dynamic ESG</div>
                                    <div className={classes.coverTitle} > Investment</div>
                                    <div className={classes.coverTitle} >Insights</div>
                                </div>
                                <div className={classes.coverTitleSection} >
                                    <div className={classes.coverSubTitle} >Powered by AI</div>
                                    <div className={classes.coverSubTitle} >Guided by domain experts</div>
                                </div>
                            </div>
                        </Grid> */}
                    </Grid>
                </Grid>
            </Container>
        </div>
    )
}