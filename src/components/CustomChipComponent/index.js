import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Chip, Paper, Tooltip } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        // justifyContent: 'left',
        justifyContent: props => props.chipAlign || 'left',
        flexWrap: 'wrap',
        listStyle: 'none',
        padding: theme.spacing(0.5),
        margin: 0,
        background: props => props.backgroundColor || "white"
    },
    chip: {
        margin: theme.spacing(0.5),
        // textOverflow:'ellipsis',
        // maxWidth:theme.spacing(8)
    },
}));

function CustomChipComponent(props) {
    const classes = useStyles(props);

    return (
        <Paper component="ul" className={classes.root} elevation={0}>
            {props.chipData && props.chipData.length > 0 && props.chipData.map((data, index) => (
                data[props.labelkey] && <li key={index}>
                    <Tooltip title={props.chipLabelLetterSize ? data[props.labelkey] : ""} placement="top">
                        <Chip
                            size='small'
                            clickable={false}
                            className={classes.chip}
                            color={props.chipColor}
                            variant={props.variant || 'default'}
                            icon={data.icon ? data.icon : undefined}
                            label={props.chipLabelLetterSize ? `${data[props.labelkey].substring(0, props.chipLabelLetterSize)} ...` : data[props.labelkey]}
                            onDelete={(event) => props.handleDelete ? props.handleDelete(event, data) : null}
                        />
                    </Tooltip>
                </li>
            ))}
        </Paper>
    );
}

export default CustomChipComponent