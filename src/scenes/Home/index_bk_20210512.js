import React from 'react';

import { makeStyles } from '@material-ui/core/styles';

import coverImage from '../../assets/environment.jpg'

const useStyles = makeStyles((theme) => ({
    nav: {
        display: "flex",
        justifyContent: "center",
        padding: theme.spacing(10, 1, 0, 0)
    },
    navPrimeButton: {
        color: 'black',
        cursor: 'pointer'
    },
    navButton: {
        marginLeft: theme.spacing(4),
        color: 'black',
        cursor: 'pointer'
    },
    image:{
        position:'relative',
        padding: theme.spacing(4),
        height:"500px",
        width:"1200px"
    }
}))

export default function Navbar(props) {
    const classes = useStyles();

    return (
        <>
            <div className={classes.nav}>
                <div className={classes.navPrimeButton}>HOME</div>
                <div className={classes.navButton}>TEAM/LEADERSHIP</div>
                <div className={classes.navButton}>PRODUCTS</div>
                <div className={classes.navButton}>CONTACT US</div>
                <div className={classes.navButton} onClick={() => props.history.push('/login')}>SIGN IN</div>
            </div>
            <div>
                <img src={coverImage} alt='cover' className={classes.image} />
            </div>
        </>
    )
}