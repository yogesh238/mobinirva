import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Card, Paper, CardContent, CardMedia, Typography } from '@material-ui/core';

import c1 from '../../assets/c1.jpg'
import c2 from '../../assets/c2.jpg'
import c3 from '../../assets/c3.jpg'

const controversies = [
    {
        image: c1,
        data: 'South Africa launches fund to boost Black farming',
        time: '2 hours ago'
    },
    {
        image: c2,
        data: 'Analysis: Sheen comes of green in crowded climate investment space',
        time: '5 hours ago'
    },
    {
        image: c3,
        data: "Apple Inc spending from 'green bonds' hits $2.8 billion",
        time: 'March 11'
    },
    {
        image: c2,
        data: "Apple Inc spending from 'green bonds' hits $2.8 billion",
        time: 'March 11'
    },
]

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        width: 500,
        height: 100,
        marginBottom: 3
    },
    paper: {
        padding: theme.spacing(2, 0, 0, 2)
    },
    cover: {
        marginTop: theme.spacing(1),
        height: 80,
        minWidth: 100,
    },
    details: {
        display: 'flex',
        flexDirection: 'column',
    },
    content: {
        flex: '1 0 auto',
        maxHeight: 28,
        fontSize: 14
        // marginBottom:theme.spacing(1)
    },
    controls: {
        paddingLeft: theme.spacing(2),
        marginBottom: theme.spacing(2),
    },
}));

export default function Controversies(props) {
    const classes = useStyles();

    return (
        <>
            <Typography variant="subtitle1">
                Controversies
            </Typography>
            <Paper className={classes.paper}>
                {controversies.map(controversy => (
                    <Card className={classes.root}>
                        <CardMedia
                            className={classes.cover}
                            image={controversy.image}
                        />
                        <div className={classes.details}>
                            <CardContent >
                                <Typography variant="p" display='block' className={classes.content}>
                                    {controversy.data}
                                </Typography>
                            </CardContent>
                            <div className={classes.controls}>
                                <Typography variant="caption" color="textSecondary">
                                    {controversy.time}
                                </Typography>
                            </div>
                        </div>
                    </Card>
                ))}
            </Paper>
        </>
    );
}