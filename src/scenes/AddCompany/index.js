import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import { Button, Grid, Paper, Typography } from '@material-ui/core';

import CustomModal from '../../components/Modal/CustomModal';
import CustomSearchBar from '../../components/CustomSearchBar';
import CustomChipComponent from '../../components/CustomChipComponent'

import CompanyBySectorTree from './CompanyBySector';
import CompanyByLocationTree from './CompanyByLocation';

import { allCompanies } from './addCompanyJsons'
import companiesBySector from './companiesBySector.json';
import companiesByLocation from './companiesByLocation.json';

const useStyles = makeStyles((theme) => ({
    addCompanyButton: {
        textTransform: 'none',
        fontSize: 14,
        // marginRight: theme.spacing(3)
    },
}))

function AddCompany(props) {
    const classes = useStyles()

    const [modalOpen, setModalOpen] = React.useState(false);
    const [companies, setCompanies] = React.useState([]);
    const [searchValue, setSearchValue] = React.useState('');
    const [suggestions, setSuggestions] = React.useState([]);
    const [searchedCompany, setSearchedCompany] = React.useState({});

    const handleModalClose = () => {
        setModalOpen(false);
        // setCompanies([])
        setSuggestions([])
        setSearchValue('')
        setSearchedCompany({})
    }

    const onAddHandler = (event) => {
        props.addHandler(event, companies)
        handleModalClose();
    }

    React.useEffect(() => {
        if (props.defaultCompanies && props.defaultCompanies.length) {
            let companies = props.defaultCompanies.map(comp => ({ companyid: comp.companyid, companyname: comp.companyname }))
            setCompanies(companies)
        }
    }, [props.defaultCompanies])

    const onSearch = (event) => {
        if (event.target && event.target.value) {
            setSearchValue(event.target.value)
            let s = allCompanies.filter(company => company.companyname.toLowerCase().includes(event.target.value.toLowerCase()))
            setSuggestions(s)
        } else {
            setSearchValue('')
            setSuggestions([])
        }
    }

    const onSuggestion = (event, suggestion) => {
        setSearchValue(suggestion.companyname)
        setSuggestions([])
        setSearchedCompany(suggestion)
    }

    // this function will be called whenever you check or uncheck a company
    const onCompanyClick = (event, value) => {
        if (event.target && event.target.checked) {
            setCompanies([...companies, value])
        } else {
            setCompanies(companies.filter(company => company.companyid !== value.companyid))
        }
    }

    // this function will be called whenever you check or uncheck any parent node
    const onParentClick = (event, value) => {
        if (event.target.checked) {
            if (value && value.length) {
                setCompanies([...companies, ...value])
            }
        } else {
            if (value && value.length) {
                let filterCompanies = companies.filter(company => !value.some(value => value.companyid === company.companyid))
                setCompanies(filterCompanies && filterCompanies.length > 0 ? filterCompanies : [])
            }
        }
    }

    React.useEffect(() => {
        // testing array functions
        // let main = [1, 2, 3, 4, 5, 6, 7, 3, 2, 6, 5]
        let main = [{ id: 1, name: 'a' }, { id: 2, name: 'a' }, { id: 1, name: 'a' }, { id: 2, name: 'b' }, { id: 2, name: 'b' }]
        let sub = [4, 5, 6]
        // let d = main.filter(m => sub.indexOf(m) === -1)
        // let d = [...new Set(main)]
        main = [...new Set([...main, { id: 2, name: 'c' }].map(m => JSON.stringify(m)))].map(s => JSON.parse(s))
        // console.log("it is main", main);
    }, [])

    return (
        <>
            <div>
                <Button
                    variant="contained"
                    color="primary"
                    size="large"
                    className={classes.addCompanyButton}
                    onClick={() => { setModalOpen(true) }}
                >
                    {'+ Add Company'}
                </Button>
            </div>
            <CustomModal
                title="Add Company"
                open={modalOpen}
                maxWidth='md'
                onClose={handleModalClose}
                onAdd={onAddHandler}
            >
                <Grid container spacing={3}>
                    <Grid item xs={12} >
                        {/* <Paper className={classes.paper}>{companies[companies.length - 1].companyname}</Paper> */}
                        <CustomChipComponent
                            chipData={companies}
                            labelkey='companyname'
                            chipColor='primary'
                            chipAlign='left'
                            chipLabelLetterSize={20}
                            handleDelete={onCompanyClick}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <CustomSearchBar
                            borderRadius={15}
                            width={'34%'}
                            height={40}
                            fontSize={18}
                            iconSize='medium'
                            iconRight={false}
                            placeholder="Search"
                            value={searchValue}
                            onChange={onSearch}
                            suggestions={suggestions}
                            suggestionLabelKey={'companyname'}
                            onSuggestion={onSuggestion}
                        // icon={null}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <Grid container spacing={3}>
                            <Grid item xs={6}>
                                <Typography variant="caption" component="p" >Search company by location </Typography>
                                <CompanyByLocationTree data={companiesByLocation} selectedCompanies={companies} onCompanyClick={onCompanyClick} onParentClick={onParentClick} searchedCompany={searchedCompany} />
                            </Grid>
                            <Grid item xs={6}>
                                <Typography variant="caption" component="p" >Search company by Industry </Typography>
                                <CompanyBySectorTree data={companiesBySector} selectedCompanies={companies} onCompanyClick={onCompanyClick} onParentClick={onParentClick} searchedCompany={searchedCompany} />
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </CustomModal>
        </>
    )
}

export default AddCompany