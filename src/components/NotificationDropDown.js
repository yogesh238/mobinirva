import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grow from '@material-ui/core/Grow';
import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import Badge from '@material-ui/core/Badge';
import { makeStyles } from '@material-ui/core/styles';
import NotificationsIcon from '@material-ui/icons/Notifications';

import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import NotificationsNoneOutlinedIcon from '@material-ui/icons/NotificationsNoneOutlined';
const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    zIndex:99,
  },
  paper: {
    marginRight: theme.spacing(2),
  },
  listroot: {
    width: '100%',
    maxWidth: '36ch',
    backgroundColor: theme.palette.background.paper,
    
  },
  inline: {
    display: 'inline',
  },
}));

export default function NotificationDropDown() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const anchorRef = React.useRef(null);

  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen);
  };

  const handleClose = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }

    setOpen(false);
  };

 

  // return focus to the button when we transitioned from !open -> open
  const prevOpen = React.useRef(open);
  React.useEffect(() => {
    if (prevOpen.current === true && open === false) {
      anchorRef.current.focus();
    }

    prevOpen.current = open;
  }, [open]);

  return (
    <div className={classes.root}>
      
      <IconButton 
        onClick={handleToggle}
        ref={anchorRef}

        color="inherit">
        <Badge badgeContent={1} color="secondary">
            <NotificationsIcon />
        </Badge>
    </IconButton>
       
        <Popper open={open} anchorEl={anchorRef.current} role={undefined} transition disablePortal
        placement='top-end'
        >
          {({ TransitionProps, placement }) => (
            <Grow
              {...TransitionProps}
              style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
            >
              <Paper>
                <ClickAwayListener onClickAway={handleClose}>
                <List className={classes.listroot}>
                    <ListItem alignItems="flex-start">
                        <ListItemText
                        primary={
                            <React.Fragment>
                            <Typography
                                component="span"
                                variant="body2"
                                className={classes.inline}
                                color="textPrimary"
                            >
                                Alerts
                            </Typography>
                            </React.Fragment>
                        }
                        secondary=""
                        />
                        <ListItemSecondaryAction>
                            <IconButton edge="end" aria-label="delete">
                                <CloseIcon onClick={handleToggle} />
                            </IconButton>
                        </ListItemSecondaryAction>
                    </ListItem>
                    <Divider />
                    <ListItem alignItems="flex-start" style={{borderLeft:'2px solid #0A67D4'}}>
                        <IconButton edge="start">
                            <NotificationsNoneOutlinedIcon />
                        </IconButton>
                        <ListItemText
                        primary={
                            <React.Fragment>
                            <Typography
                                component="span"
                                variant="body2"
                                className={classes.inline}
                                color="textPrimary"
                            >
                                Nirva Dynamic rating/outlook change
                            </Typography>
                            </React.Fragment>
                        }
                        secondary="5 mins ago"
                        />
                    </ListItem>
                    {/* <Divider /> */}
                </List>
                </ClickAwayListener>
              </Paper>
            </Grow>
          )}
        </Popper>
    </div>
  );
}



