import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import { Card, CardContent, Typography } from '@material-ui/core';

const latestNewses = [
    {
        data: 'Nirva Dynamic rating/outlook change',
        time: '2 hours ago'
    },
    {
        data: 'Nirva adds geo-spetial data',
        time: '5 hours ago'
    },
    {
        data: "Nirva AI Guided Materiality",
        time: 'March 11'
    },
    {
        data: "Nirva Taxonomy change",
        time: 'March 11'
    },
    {
        data: "Nirva adds more historical data",
        time: 'March 11'
    }
]

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        width: 400,
        height: 70,
        marginBottom: theme.spacing(1),
        paddingBottom: theme.spacing(1),
    },
    details: {
        display: 'flex',
        flexDirection: 'column',
    },
    content: {
        flex: '1 0 auto',
        maxHeight: 24,
        fontSize: 14
    },
    controls: {
        paddingLeft: theme.spacing(2),
        marginBottom: theme.spacing(2),
    },
}));

export default function Controversies(props) {
    const classes = useStyles();

    return (
        <>
            <Typography variant="subtitle1">
                Latest at Nirva
            </Typography>
            {
                latestNewses.map(latestNews => (
                    <Card className={classes.root}>
                        <div className={classes.details}>
                            <CardContent>
                                <Typography variant="p" display='block' className={classes.content}>
                                    {latestNews.data}
                                </Typography>
                            </CardContent>
                            <div className={classes.controls}>
                                <Typography variant="caption" color="textSecondary">
                                    {latestNews.time}
                                </Typography>
                            </div>
                        </div>
                    </Card>
                ))
            }
        </>
    );
}