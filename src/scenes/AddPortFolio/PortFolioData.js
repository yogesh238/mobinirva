export const DATA = [
    {
        portfolioid: 1,
        portfolioname: 'Portfolio A',
        mandateid: 'm1',
        mandatename: 'Mandate A',
        companies: [
            {
                "companyid": 49,
                "companyname": "Colgate-Palmolive India Ltd"
            },
            {
                "companyid": 50,
                "companyname": "Dabur India Ltd"
            }
        ]
    },
    {
        portfolioid: 2,
        portfolioname: 'Portfolio B',
        mandateid: 'm2',
        mandatename: 'Mandate B',
        companies: [
            {
                "companyid": 51,
                "companyname": "Marico Ltd"
            },
            {
                "companyid": 52,
                "companyname": "Godrej Consumer Products Ltd"
            },
            {
                "companyid": 53,
                "companyname": "Emami Ltd"
            }
        ]
    },
    {
        portfolioid: 3,
        portfolioname: 'Portfolio C',
        mandateid: 'm3',
        mandatename: 'Mandate C',
        companies: [
            {
                "companyid": 49,
                "companyname": "Colgate-Palmolive India Ltd"
            },
            {
                "companyid": 50,
                "companyname": "Dabur India Ltd"
            },
            {
                "companyid": 51,
                "companyname": "Marico Ltd"
            },
            {
                "companyid": 52,
                "companyname": "Godrej Consumer Products Ltd"
            },
            {
                "companyid": 53,
                "companyname": "Emami Ltd"
            },
            {
                "companyid": 59,
                "companyname": "ITC Ltd"
            }
        ]
    },
]

export const MANDATEDATA = [
    {
        mandateid: 'm1',
        mandatename: 'Mandate A',
    },
    {
        mandateid: 'm2',
        mandatename: 'Mandate B',
    },
    {
        mandateid: 'm3',
        mandatename: 'Mandate C',
    },
    {
        mandateid: 'm4',
        mandatename: 'Mandate D',
    },
]