import React from "react";

// import { withAuthenticator, AmplifySignOut } from '@aws-amplify/ui-react';
import { 
  ConfirmSignIn, 
  ConfirmSignUp, 
  ForgotPassword, 
  RequireNewPassword, 
  SignUp, 
  VerifyContact, 
  withAuthenticator ,

  // Authenticator,
  // SignIn,
  // Greetings,
} from 'aws-amplify-react';



import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";

import NirvaHome from './scenes/NirvaHome'
import FlexiMandate from './scenes/FlexiMandate'
import Profile from './scenes/Profile'
import ControversyModule from './scenes/ControversyModule'
import './App.css'
import Login from './scenes/Login'

const ProtectedRoutes = (props) => {
  return (
    <Router>
      <Switch>
          <Route exact path='/profile' component={Profile} />
          <Route exact path='/nirvahome' component={NirvaHome} />
          <Route exact path='/fleximandate' component={FlexiMandate} />
          <Route exact path='/controversy' component={ControversyModule} />
      </Switch>
  </Router>
  );
};


export default withAuthenticator(ProtectedRoutes, false,  [
  <ConfirmSignIn/>,
  <VerifyContact/>,
  <Login/>,
  <SignUp/>,
  <ConfirmSignUp/>,
  <ForgotPassword/>,
  <RequireNewPassword />
]);
