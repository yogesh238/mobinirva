import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import { Button, CssBaseline, TextField, Link, Paper, Grid, Typography } from '@material-ui/core';

import loginImage from '../../assets/login.jpg'

const useStyles = makeStyles((theme) => ({
    root: {
        height: '100vh',
    },
    image: {
        backgroundImage: `url(${loginImage})`,
        backgroundRepeat: 'no-repeat',
        backgroundColor:
            theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
        backgroundSize: 'cover',
        backgroundPosition: 'center',
    },
    paper: {
        margin: theme.spacing(20, 4),
        display: 'flex',
        flexDirection: 'column',
        // alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    forgotPassword: {
        marginTop: theme.spacing(3),
    },
    login: {
        margin: theme.spacing(3, 0, 2),
    },
    signup: {
        margin: theme.spacing(3),
    },
}));

export default function Login(props) {
    const classes = useStyles();

    return (
        <Grid container component="main" className={classes.root}>
            <CssBaseline />
            <Grid item xs={false} sm={4} md={7} className={classes.image} />
            <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
                <div className={classes.paper}>
                    <Typography component="h1" variant="h5">
                        Login to your account
                    </Typography>
                    <form className={classes.form} noValidate>
                        <TextField
                            margin="normal"
                            required
                            fullWidth
                            id="username"
                            label="Username"
                            name="username"
                            autoComplete="off"
                            autoFocus
                        />
                        <TextField
                            margin="normal"
                            required
                            fullWidth
                            name="password"
                            label="Password"
                            type="password"
                            id="password"
                            autoComplete="off"
                        />
                        <Grid container>
                            <Grid item xs className={classes.forgotPassword}>
                                <Link href="#" variant="body2">
                                    Forgot password?
                                </Link>
                            </Grid>
                        </Grid>

                        <Button
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                            onClick={() => props.history.push('/nirvahome')}
                        >
                            Login
                        </Button>
                        <Button
                            variant="outlined"
                            color="primary"
                            className={classes.signup}
                        >
                            Create Account
                        </Button>
                    </form>
                </div>
            </Grid>
        </Grid>
    );
}