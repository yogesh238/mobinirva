import React from 'react';

import Carousel, { slidesToShowPlugin } from '@brainhubeu/react-carousel';
import '@brainhubeu/react-carousel/lib/style.css';

import Card from './Card';

const ShowCarousel = ({ data, ...props }) => (
    <Carousel
        plugins={[
            // 'infinite',
            // 'arrows',
            {
                resolve: slidesToShowPlugin,
                options: {
                    numberOfSlides: 3
                }
            },
        ]}
    >
        {data && data.map(item => (
            <Card
                key={item.id}
                imgURL={item.imgURL}
                imgAlt={item.title}
                // title={item.title}
            />
        ))}
    </Carousel>
);

export default ShowCarousel;