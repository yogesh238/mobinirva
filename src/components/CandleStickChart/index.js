import React from 'react'
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';

import CandleStickBar from './CandleStickBar';

const useStyles = makeStyles((theme) => ({
    container: {
        display: 'flex',
        flexDirection: 'column',
        // alignItems: 'center',
        // justifyContent:'center',
        margin: 10,
        // backgroundColor: 'green'
    },
    item: {
        display: 'flex',
        flexGrow: 1,
        justifyContent: 'space-between',
        alignItems: 'center',
        borderRadius: 5,
        paddingLeft: 10,
        paddingRight: 10,
        // padding: 5,
    },
    item_bgc_odd: {
        backgroundColor: '#F0F1F3'
    },
    item_bgc_even: {
        backgroundColor: '#FFFFFF'
    },

}))

function CandleStickChart({ data }) {

    const classes = useStyles();

    return (
        <div className={classes.container}>
            { data && Array.isArray(data) && data.map((item, i) => (
                <div className={`${classes.item} ${i % 2 === 0 ? classes.item_bgc_even : classes.item_bgc_odd}`}>
                    <div>{item.sector}</div>
                    <CandleStickBar width={300} height={50} min={item.min} max={item.max} position={item.position} />
                </div>
            ))}
        </div>
    )
}

CandleStickChart.propTypes = {
    data: PropTypes.array.isRequired
}

export default CandleStickChart

