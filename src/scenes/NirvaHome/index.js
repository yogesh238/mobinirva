import React, { useState } from 'react';

import { Typography, Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import {
    Container,
    // Paper,
    Button, Card, CardActions, CardContent
} from '@material-ui/core';

import Carousel, { slidesToShowPlugin } from '@brainhubeu/react-carousel';
import '@brainhubeu/react-carousel/lib/style.css';

import { clsx } from '../../utils/combineClassNames';

import Navbar from '../../components/Navbar'
import Searchbar from '../../components/Searchbar'

import LatestNews from './LatestNews'
import Controversies from './Controversies'
import PopularSearchImageCard from './PopularSearches'

import CustomImageCard from './CustomImageCard';
import CustomBarGraph from '../../components/CustomBarGraph';
import CustomDataTable from '../../components/CustomDataTable';


import NirvaHomeEnvironment from './nirvaHomeEnvironment';
import NirvaHomeIndustry from './nirvaHomeIndustry';
import NirvaHomePeer from './nirvaHomePeer';
import NirvaHomeStrategy from './nirvaHomeStrategy';
import NirvaHomeTarget from './nirvaHomeTarget';

import { loginData, popularSearches, tableHeaders, emissionTableData, emissionGraphData, revenueGraphData, corouselData, ghgCompanyDetails } from './nirvahomeJsons'

const useStyles = makeStyles((theme) => ({
    root: {
        // display: 'flex',
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    main: {
        backgroundColor: '#F7F7FB',
        minHeight: '100vh',
        height: '100%',
    },
    secondaryDiv: {
        padding: theme.spacing(4, 20, 0, 20)
    },
    paddingDiv: {
        // border: '1px solid black',
        padding: theme.spacing(1, 0, 2, 0),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    },
    searchDiv: {
        marginTop: theme.spacing(1)
    },
    popularSearches: {
        marginTop: theme.spacing(3),
        width: 500,
        minHeight: 140,
        // border: '1px solid blue',
    },
    imagesGrid: {
        marginTop: 2,
    },
    bottomDiv: {
        marginTop: theme.spacing(5),
        display: "flex"
    },
    controversies: {
        // border: '1px solid red'
    },
    latestNirva: {
        // border: '1px solid black',
        marginLeft: theme.spacing(2)
    },
    searchDataTable: {
        margin: theme.spacing(4, 0, 0, 0),
    },
    searchDataGraph: {
        width: 800,
        // height: 500,
        marginTop: theme.spacing(2),
        display: 'flex'
    },
    emissionGraph: {
        width: 400
    },
    revenueGraph: {
        width: 400,
        marginLeft: theme.spacing(2)
    },

    // kumar styles
    row: {
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2),
    },
    bgY: {
        backgroundColor: 'yellow'
    },
    titlePrimary: {
        display: 'flex',
        marginTop: theme.spacing(8),
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: theme.spacing(2),
    },
    dfCenter: {
        // display flex center
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    w50: {
        width: '50%',
    },
    popSearches: {
        display: 'flex',
        flexDirection: 'column',
        // width: '50%',
        marginTop: theme.spacing(4),
    },
    searchTitle: {
        color: '#0A67D4',
        fontSize: 26,
        fontWeight: 'bold',
        // marginBottom: theme.spacing(2),
        // lineHeight: 27,
        // textAlign: 'center',
        // backgroundColor: 'yellow'
    },
    title: {
        color: '#131111',
        fontSize: 22,
        fontWeight: 'bold',
        marginBottom: theme.spacing(2),
        // lineHeight: 27,
        // textAlign: 'center',
        // backgroundColor: 'yellow'
    },
    card: {
        // display: 'flex',
        // alignItems: 'center',
        // justifyContent: 'center',
        borderRadius: 1,
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2),
        padding: theme.spacing(1),
    },
    graphCard: {
        marginLeft: theme.spacing(1),
        margin: theme.spacing(2)
    },
    graphTitle: {
        color: '#131111',
        fontSize: 20,
        fontWeight: '600',
        marginBottom: theme.spacing(2),
        marginTop: theme.spacing(3),
        marginLeft: theme.spacing(3),
    },
    exportActionButton: {
        textTransform: 'none',
        fontSize: 14,
        marginRight: theme.spacing(3)
    }
}))

function ActionComponent(props) {
    const classes = useStyles()
    return (
        <Button
            variant="outlined"
            color="primary"
            size="small"
            className={classes.exportActionButton}
        >
            {'Export to pdf'}
        </Button>
    )
}

export default function NirvaHome(props) {
    const classes = useStyles()

    const [searchValue, setSearchValue] = useState('')
    const [condition, setCondition] = useState('')

    const onPopularSearchClick = (event, search, key) => {
        setCondition(key)
        setSearchValue(search)
    }

    return (
        <div className={classes.root}>
            <Navbar />
            <Container maxWidth="lg" >
                <Grid
                    container
                    direction="row"
                    justify="center"
                    alignItems="center"
                    spacing={2}
                    className={classes.row}
                >
                    <Grid container xs={6}>
                        <Grid item xs={12}
                            className={classes.titlePrimary}
                        >
                            <Typography variant="h6" color='primary' className={classes.searchTitle}>Hi {loginData.username}, How Can We Help?</Typography>
                        </Grid>
                        <Grid item xs={12}
                            className={classes.dfCenter}
                        >
                            <Searchbar value={searchValue} />
                        </Grid>

                        <Grid item xs={12}
                            className={classes.popSearches}
                        >
                            <Typography variant="subtitle2" color='textSecondary'>
                                Popular Searches
                            </Typography>
                            <Grid container>
                                {popularSearches && popularSearches.map(item => (
                                    <PopularSearchImageCard
                                        key={item.id}
                                        imgURL={item.imgURL}
                                        imgAlt={item.title}
                                        title={item.title}
                                        searchKey={item.key}
                                        searchWord={item.search}
                                        imgHeight={60}
                                        onClick={onPopularSearchClick}
                                    />
                                ))}
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>

                {!condition &&
                    <Grid
                        container
                        direction="row"
                        justify="center"
                        alignItems="center"
                        spacing={2}
                        className={classes.row}
                    >
                        <div className={classes.bottomDiv}>
                            <div className={classes.controversies}>
                                <Controversies />
                            </div>
                            <div className={classes.latestNirva}>
                                <LatestNews />
                            </div>
                        </div>
                    </Grid>
                }
                {condition && condition === 'emission' &&
                    <Grid
                        container
                        direction="row"
                        justify="center"
                        alignItems="center"
                        spacing={2}
                        className={classes.row}
                    // xs={10}
                    >
                        <Grid item xs={12}>
                            <Typography variant="subtitle2" color='textSecondary'>
                                Showing GHG Emission of Company AB from 2015-2021
                            </Typography>
                        </Grid>
                        <Grid item xs={12}>
                            <CustomDataTable
                                title='Company AB - GHG Emissions'
                                headerData={tableHeaders}
                                tableData={emissionTableData}
                                actions={<ActionComponent />}
                                sortable={false}
                            />
                        </Grid>
                        <Grid
                            // item xs={12}
                            container
                        // alignContent="space-around"
                        // justify="space-evenly"
                        // spacing={2}
                        // className={classes.row}
                        >
                            <Grid item lg={6} md={6} xs={12}>
                                <Card
                                    className={clsx([classes.card, classes.graphCard])}
                                    elevation={4}
                                >
                                    <CardContent>
                                        <Typography component="p" variant="p" className={classes.graphTitle}>
                                            GHG Emission of Company AB from 2017 -2021
                                    </Typography>
                                        <CustomBarGraph
                                            barGraphName='emission'
                                            data={emissionGraphData}
                                            xAxisLabel='year'
                                            yAxisLabel='GHG Emission'
                                        />
                                    </CardContent>
                                </Card>
                            </Grid>
                            <Grid item lg={6} md={6} xs={12}>
                                <Card
                                    className={clsx([classes.card, classes.graphCard])}
                                    elevation={4}
                                >
                                    <CardContent>
                                        <Typography component="p" variant="p" className={classes.graphTitle}>
                                            GHG//Revenue of Company AB from 2017 -2021
                                        </Typography>
                                        <CustomBarGraph
                                            barGraphName='revenue'
                                            data={revenueGraphData}
                                            xAxisLabel='year'
                                            yAxisLabel='GHG/Revenue'
                                        />
                                    </CardContent>
                                </Card>
                            </Grid>
                        </Grid>
                        <Grid item xs={12}>
                            <Card
                                className={classes.card}
                                elevation={4}
                            >
                                <CardContent>
                                    <Typography component="p" variant="p" className={classes.title}>
                                        Company Description
                                    </Typography>
                                    <Typography
                                        component="p">
                                        {ghgCompanyDetails}
                                    </Typography>
                                </CardContent>
                                <CardActions>
                                    <Button style={{ textTransform: 'none' }} size="small" href="#text-buttons" color="primary">
                                        {'more>>'}
                                    </Button>
                                </CardActions>
                            </Card>
                        </Grid>
                        <Grid item xs={12}>
                            <Typography variant="p" className={classes.title} >
                                Do You Know?
                            </Typography>
                            {/* <Card
                                className={classes.card}
                                elevation={4}
                            >
                                <CardContent> */}
                            <Carousel
                                plugins={[
                                    // 'infinite',
                                    // 'arrows',
                                    {
                                        resolve: slidesToShowPlugin,
                                        options: {
                                            numberOfSlides: 3
                                        }
                                    },
                                ]}
                            >
                                {corouselData && corouselData.map((item, index) => (
                                    <CustomImageCard
                                        key={index}
                                        imgURL={item.imgURL}
                                        imgAlt={item.title}
                                    // title={item.title}
                                    />
                                ))}
                            </Carousel>
                            {/* </CardContent>
                            </Card> */}
                        </Grid>
                    </Grid>
                }
                {condition && condition === 'peer' &&
                <NirvaHomePeer />
                }
                {condition && condition === 'target' &&
                <NirvaHomeTarget />
                }
                {condition && condition === 'industry' &&
                <NirvaHomeIndustry />
                }
                {condition && condition === 'environment' &&
                <NirvaHomeEnvironment />
                }
                {condition && condition === 'strategy' &&
                <NirvaHomeStrategy />
                }
            </Container>
        </div >
    );
}