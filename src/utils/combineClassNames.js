
export const clsx = (array) => {
    let str = '';
    if (Array.isArray(array)) {
        let len = array.length;
        array.forEach((clss, i) => {
            if (typeof clss === 'string') {
                str = str + clss;
                if (i !== len - 1) {
                    str = str + ', ';
                }
            }
        });
    }
    return str;
}