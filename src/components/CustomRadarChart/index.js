import React from "react";
import ReactApexChart from "react-apexcharts";

function CustomRadarChart(props) {
    const { height = 400, data = [], labels = [], maxValue = 100, miniChart = false, numberOfGrids = 7, gridColors = [] } = props;

    let gData = {
        series: [{
            name: 'Esg Dimension',
            data: [50, 50, 60, 40, 70],
        }],
        options: {
            chart: {
                height: 'auto',
                width: '100%',
                type: 'radar',
                toolbar: {
                    show: false,
                }
            },
            fill: {
                opacity: 0.5
            },
            markers: {
                colors: 'White',
                size: 2
            },
            plotOptions: {
                radar: {
                    size: undefined,
                    offsetX: 0,
                    offsetY: 0,
                    polygons: {
                        strokeColors: '#e8e8e8',
                        connectorColors: '#87ceeb',
                        fill: {
                            // colors: ['#ffffff', '#66DA26', '#546E7A', '#E91E63', '#FF9800', '#ffffff', 'pink'],
                            colors: [],
                        }
                    },
                }
            },
            xaxis: {
                categories: ['Environment', 'Social Capital', 'Human Capital', 'Business Model', 'LeaderShip'],
                labels: {
                    show: true,
                },
                offsetX: 0,
                offsetY: 0,
            },
            yaxis: {
                show: false,
                min: 0,
                max: 100,
                tickAmount: 7,
                labels: {
                    show: true,
                }
            },
            tooltip: {
                enabled: false,
            }
        },
    };

    const [graphData, setGraphData] = React.useState(gData)

    React.useEffect(() => {
        let gData = {
            series: [{
                name: 'Esg Dimension',
                data: data,
            }],
            options: {
                chart: {
                    height: 'auto',
                    width: miniChart ? '50%' : '100%',
                    offsetX: 0,
                    offsetY: 0,
                    type: 'radar',
                    toolbar: {
                        show: false,
                    }
                },
                fill: {
                    opacity: 0.5
                },
                markers: {
                    colors: 'White',
                    size: 2
                },
                plotOptions: {
                    radar: {
                        size: undefined,
                        offsetX: 0,
                        offsetY: 0,
                        polygons: {
                            strokeColors: '#e8e8e8',
                            connectorColors: '#87ceeb',
                            fill: {
                                // colors: ['#ffffff', '#66DA26', '#546E7A', '#E91E63', '#FF9800', '#ffffff', 'pink'],
                                colors: [],
                            }
                        },
                    }
                },
                xaxis: {
                    // categories: ['Environment', 'Social Capital', 'Human Capital', 'Business Model', 'LeaderShip'],
                    categories: labels,
                    labels: {
                        show: !miniChart,
                    }
                },
                yaxis: {
                    show: false,
                    min: 0,
                    max: maxValue,
                    tickAmount: numberOfGrids,
                    labels: {
                        show: true,
                    }
                },
                tooltip: {
                    enabled: false,
                }
            },
        };
        setGraphData(gData)
    }, [props])

    return (
        <div id="chart">
            <ReactApexChart options={graphData.options} series={graphData.series} type="radar" height={height} />
        </div>
    );
}

export default CustomRadarChart;